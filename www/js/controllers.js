angular.module('starter.controllers', ['ngCordova'])

.controller('LoginCtrl',function(database, $filter, $rootScope, $state, $scope, $http, UserService, $q, Url, $ionicPopup, $ionicLoading, $ionicPlatform, $ionicHistory){

  $scope.show = {};
  $scope.show.is_loaded = false;

  $scope.$on('$ionicView.enter', function(){
    //alert('EnterLogin');

    $ionicLoading.show();

    $ionicPlatform.ready(function() {
      database.createTables();
      $ionicLoading.hide();

      database.select('user').then(function(user){
        //alert('login user ' + JSON.stringify(user));
        if(user !== undefined && user.length !== 0){
          //alert(JSON.stringify(user.item(0)));

          if(user.item(0).is_logged === 'true' && user.item(0).nivel !== null){
            $state.go('tab.cards');
          }else{
            $scope.show.is_loaded = true;
          }
        }else{
          $scope.show.is_loaded = true;
        }
      }
      , function(err){
        $scope.show.is_loaded = true;
        //alert('Err ionic enter login ' + JSON.stringify(err));
      }
      );
      
      $rootScope.leaveapp = true;
    });
  });

  // $scope.$on('$ionicView.leave', function(){
  //   database.deleteDB();
  // });

  $rootScope.showAlert = function(titulo, msg) {
    var alertPopup = $ionicPopup.alert({
      title: titulo,
      template: msg,
      cssClass: 'customAlertCss'
    });
    alertPopup;
  };

  function checkUserConfig(user){
    //alert('checkUserConfig');
    //alert('nivel ' + user.nivel);
    //alert(JSON.stringify(user));
    UserService.setUser(user);

    if(user.nivel == null){
      $state.go('tab.autorizar');
    }else{
      $state.go('tab.cards');
    }
  }

  $rootScope.checkPermission = function() {
    var info = $q.defer();

    askPermissions = function() {
      var isRequesting = cordova.plugins.diagnostic.isRequestingPermission();
      if(!isRequesting){
        cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
          for (var permission in statuses){
            switch(statuses[permission]){
              case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                info.resolve();
                break;
              case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                break;
              case cordova.plugins.diagnostic.permissionStatus.DENIED:
                break;
              case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                break;
            }
          }
        }, function(error){
          $rootScope.showAlert('Erro ao pedir permissões, faça manualmente nas configurações do aplicativo.');
          info.reject(err);
        },[
            cordova.plugins.diagnostic.runtimePermission.ACCESS_FINE_LOCATION, 
            cordova.plugins.diagnostic.runtimePermission.ACCESS_COARSE_LOCATION, 
            cordova.plugins.diagnostic.runtimePermission.WRITE_EXTERNAL_STORAGE,
            cordova.plugins.diagnostic.runtimePermission.READ_EXTERNAL_STORAGE
        ]);
      }
    };

    cordova.plugins.diagnostic.getPermissionsAuthorizationStatus(function(statuses){
      for (var permission in statuses){
          switch(statuses[permission]){
              case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                  info.resolve();
                  break;
              case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                  askPermissions();
                  break;
              case cordova.plugins.diagnostic.permissionStatus.DENIED:
                  askPermissions();
                  break;
              case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                  askPermissions();
                  break;
          }
      }
    }, 
    function(error) {
      $rootScope.showAlert('Erro ao requisitar permissões do dispositivo');
      info.reject(error);
    },[
        cordova.plugins.diagnostic.permission.ACCESS_FINE_LOCATION, 
        cordova.plugins.diagnostic.permission.ACCESS_COARSE_LOCATION, 
        cordova.plugins.diagnostic.permission.WRITE_EXTERNAL_STORAGE,
        cordova.plugins.diagnostic.permission.READ_EXTERNAL_STORAGE
    ]);

    return info.promise;
  };

  if(navigator.connection.type == 'none'){
    var alertPopup = $ionicPopup.alert({
      title: 'Sem Internet',
      template: 'Habilite a conexão da internet no seu dispositivo para usar o Bailaki.'
    });
    alertPopup;
  }

  // verificar se o usuário existe
  var checkUserExistence = function(param){
    var deferred = $q.defer();
    $http.post(Url.url + '/api/checkUser', {param: param}).then(
      function(res){
        deferred.resolve(res.data);
      }, function(err){
        deferred.reject(err);
      });
    return deferred.promise;
  }

  $scope.loginEmail = function(){
    window.plugins.googleplus.login(
      {},
      function (data) {
        $scope.user = {};
        $scope.user.email = data.email;
        $scope.user.social_id = data.accessToken;
        $scope.user.social_network = 'google';
        $scope.user.nome = data.displayName;
        $scope.user.accessToken = data.accessToken;
        if(data.imageUrl){
          $scope.user.fotos = [data.imageUrl];
        }else{
          $scope.user.fotos = [];
        }
        $scope.user.is_logged = true;
        //alert(JSON.stringify(data));
        //UserService.setUser($scope.user);
        saveU($scope.user);
      }
    );
  }

  // salvar usuário
  var saveU = function(user){
    //alert('saveU');
    //alert(JSON.stringify(user));

    checkUserExistence(user.email).then(function(res){
      if(res.e == 1){
        //try{
          //alert('existe');

          var user_saved = angular.merge(user,res.user);
          user_saved.is_logged = true;

          const u = angular.copy(user_saved);

          //alert(typeof(u.fotos));

          //alert(JSON.stringify(u.fotos));

          if((typeof(u.fotos) == "string") && u.fotos !== null){
            u.fotos = u.fotos.replace(/"/g, "'");
          }
          u.buscar = {};
          // u.buscar.sexo_f = user.buscar_sexo_f;
          // u.buscar.sexo_m = user.buscar_sexo_m;

          // delete u.buscar_sexo_m;
          // delete u.buscar_sexo_m;
          
          //alert('u ' + JSON.stringify(u));

          database.insert('user', Object.keys(u).toString(), Object.values(u)).then(function(r){
            //alert(JSON.stringify(r));
            
            //alert('INSERT BUSCAR');

            const buscar = angular.copy(user_saved.buscar);
            //alert(JSON.stringify(buscar.sexo));
            buscar.user_id = user_saved.id;

            //alert('BUSCAR ' + JSON.stringify(buscar));

            //buscar.sexo = JSON.stringify(user_saved.buscar.sexo);
            // buscar.config = JSON.stringify(user_saved.buscar.config);
            //alert(JSON.stringify(buscar.config));
            //buscar.niveis = JSON.stringify(user_saved.buscar.niveis);
            //alert(JSON.stringify(buscar.niveis));

            database.insert('buscar', Object.keys(buscar).toString(), Object.values(buscar)).then(function(res){
              checkUserConfig(user_saved);
            }
            // , function(err){
            //   //alert('ERR BUSCAR ' + JSON.stringify(err));
            // }
            );
          }
          // , function(err){
          //   //alert('err ' + JSON.stringify(err));
          // }
          );
        // }catch(e){
        //   //alert(JSON.stringify(e));
        // }
      }else{
        saveUserData(user);
      }
    }
    // , function(err){
    //   //alert(JSON.stringify(err));
    //   //alert('Erro de conexão, tente novamente em alguns instantes');
    // }
    );
  };

  // salvar as informações do usuário
  function saveUserData(user){
    //alert('saveUserData');

    user.fotos = user.fotos.join(',').replace(/"/g, '"');

    user.buscar = {};
    // user.buscar.sexo = [];
    user.buscar.age_min = 18;
    user.buscar.age_max = 55;
    user.buscar.noti_msg = true;
    user.buscar.noti_match = true;
    user.buscar.sexo_m = true;
    user.buscar.sexo_f = true;

    // const config = [
    //   {
    //     nome: 'Matches',
    //     selected: true
    //   },
    //   {
    //     nome: 'Mensagens',
    //     selected: true
    //   }
    // ];
    var buscar = angular.copy(user.buscar);

    //user.buscar.config = JSON.stringify(config);
    
    //alert('INSERT BUSCAR 2');

    //alert('user buscar ' + JSON.stringify(user));

    $http.post(Url.url + '/api/saveUserPost', {user: angular.toJson(user)}).then(function(res){
      var _user = angular.merge(user, res.data.user);

      //alert('_user ' + JSON.stringify(_user));

      UserService.setUser(_user);

      _user.buscar = '';

      database.insert('user', Object.keys(_user).toString(), Object.values(_user)).then(function(r){
      //database.update('user', 'id', _user.id, _user).then(function(r){
        buscar.user_id = _user.id;

        //alert('buscar_ ' + JSON.stringify(buscar));

        database.insert('buscar', Object.keys(buscar).toString(), Object.values(buscar)).then(function(res){
          //alert('go autorizar');
          $state.go('tab.autorizar');   
        });
      });
    }
    // , function(err){
    //alert('ERR SaveUserPost login ' +JSON.stringify(err));
    // }
    );
    //UserService.setUser($scope.user);
  }

  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError(response);
      return;
    }
    getFacebookProfileInfo(response.authResponse)
      .then(function(data) {
        $scope.user = {};
        $scope.user.email = data.email;
        $scope.user.social_id = data.id;
        $scope.user.social_network = 'facebook';
        $scope.user.nome = data.name;
        $scope.user.accessToken = response.authResponse.accessToken;
        $scope.user.is_logged = true;
        $scope.user.fotos = ['https://graph.facebook.com/'+ data.id +'/picture?type=large'];
        //UserService.setUser($scope.user);
        //alert(JSON.stringify($scope.user));
        saveU($scope.user);
      });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    if(error.errorCode != '4201'){
      $rootScope.showAlert('Erro', 'Erro no Facebook.');
    }
    else{
      $rootScope.leaveapp = false;
    }
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();
    facebookConnectPlugin.api('/me?fields=email,name,gender,age_range,birthday&access_token=' + authResponse.accessToken, null,
      function (response) {
        info.resolve(response);
      },
      function (response) {
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the 'Login with facebook' button
  $scope.loginFacebook = function() {
    facebookConnectPlugin.getLoginStatus(function(success){      
      if(success.status === 'connected'){
        getFacebookProfileInfo(success.authResponse)
          .then(function(profileInfo) {
            $scope.user = {};
            $scope.user.email = profileInfo.email;
            $scope.user.social_id = profileInfo.id;
            $scope.user.social_network = 'facebook';
            $scope.user.nome = profileInfo.name;
            $scope.user.accessToken = success.authResponse.accessToken;
            $scope.user.is_logged = true;
            $scope.user.fotos = ['https://graph.facebook.com/'+ profileInfo.id +'/picture?type=large'];
            //UserService.setUser($scope.user);
            saveU($scope.user);
          }, function(fail){
            $rootScope.showAlert('Erro','Erro ao coletar informações do Facebook.');
          });
      } else {
        facebookConnectPlugin.login(['email, public_profile, user_birthday, user_photos, user_gender'], fbLoginSuccess, fbLoginError);
      }
    });
  };
})

.controller('AutorizarCtrl',function(database, UserService, Url, $cordovaSQLite, $http, $ionicHistory, $ionicLoading, $ionicPlatform, $ionicPopup, $rootScope, $scope, $state, $q){

  $scope.ok = {};

  $scope.$on('$ionicView.enter', function(){
    //alert('AutorizarCtrl');

    $scope.user = UserService.getUser();
    //alert('autorizar user ' + JSON.stringify($scope.user));

    $ionicPlatform.onHardwareBackButton(confirmOut);
  });
  
  var confirmOut = function() {    
    if($state.current.name === 'tab.autorizar'){
      popupShow();
    }
  }

  function popupShow(){
    var showPopupExcl = $ionicPopup.show({
      template: 'Ao sair do Bailaki você excluirá todos os dados da sua conta.',
      title: 'Tem Certeza?',
      cssClass: 'text-center',
      scope: $scope,
      buttons: [
        { 
          text: 'SIM',
          type: 'button-positive',
          onTap: function(r){
            return 0;
          }
        },
        {
          text: 'NÃO',
          type: 'button-positive',
          onTap: function(r){
            return 1;
          }
        }
      ]
    });

    showPopupExcl.then(function(res){
      if(res == 0){
        deletar();
      }
    });
  }

  $scope.goNext = function(){
    //$scope.user.autorizado = true;
    UserService.setUser($scope.user);
    $state.go('tab.sel-data');
  }

  function deletar(){
    var user = UserService.getUser();

    $http.post(Url.url + '/api/delUser', {user: angular.toJson(user)}).then(function(res){
      if(res.status == 200){
        window.plugins.OneSignal.logoutEmail(user.email);
        $scope.user = {};
        UserService.setUser({});
        UserService.empty();
        facebookConnectPlugin.logout();
        window.plugins.googleplus.logout();
        window.plugins.googleplus.disconnect();
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();

        database.deleteDB().then(function(r){
          $state.go('tab.login');
        });
      }
    }, function(res){
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache().then(function(){
        $state.go('tab.login');
      });
    });
  }

  $scope.$on('$ionicView.leave', function(){
    //$scope.user = {};
    $ionicPlatform.offHardwareBackButton(confirmOut);
  });
})

.controller('SelDataCtrl', function($filter, $scope, $state, UserService, database){
  $scope.ok = {};

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();

    //alert(JSON.stringify($scope.user));

    $scope.ok.disabled = true;
    
    if(typeof($scope.user.birthday) !== 'undefined' && $scope.user.birthday !== null && $scope.user.birthday !== ''){
      $scope.ok.disabled = false;
    }
  });

  // $scope.$on('$ionicView.leave', function(){
  //   UserService.setUser($scope.user);
  // });

  function diff_years(dt2, dt1){
    var diff =(dt2.getTime() - dt1.getTime()) / 1000;
    diff /= (60 * 60 * 24);
    return Math.abs(Math.round(diff/365.25));
  }

  $scope.verifyAge = function(){
    var diff = diff_years(new Date(), new Date($scope.user.birthday));
    if(diff > 18 && diff < 110){
      UserService.setUser($scope.user);
      $scope.ok.disabled = false;
    }else{
      $scope.ok.disabled = true;
    }
  };

  $scope.goNext = function(){
    UserService.setUser($scope.user);
    database.update('user', 'id', $scope.user.id, $scope.user).then(function(res){
      //alert(JSON.stringify(res));
      $state.go('tab.sel-sexo');
    }
    // , function(err){
    //alert(JSON.stringify(err));
    // }
    );
  }
})

.controller('SelSexoCtrlBkp',function($rootScope, $state, $scope, UserService, $q){

  $scope.ok = {};

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    $scope.ok.disabled = true;

    //alert(JSON.stringify($scope.user));

    if(typeof($scope.user.sexo) === 'undefined' || $scope.user.sexo === null){
      $scope.sexos = [
        { 
          nome: 'Homem',
          selected: false
        },
        { 
          nome: 'Mulher',
          selected: false
        }
      ];
      $scope.ok.disabled = true;
    }else{
      $scope.sexos = [
        { 
          nome: 'Homem',
          selected: $scope.user.sexo == 'Homem' ? true : false
        },
        { 
          nome: 'Mulher',
          selected: $scope.user.sexo == 'Mulher' ? true : false
        }
      ];
      $scope.ok.disabled = false;
      $scope.user.buscar.sexo = [
        {
          nome: 'Homem', 
          selected: $scope.sexos[0].selected ? false : true
        },{
          nome: 'Mulher',
          selected: $scope.sexos[1].selected ? false : true
        }
      ];
    }
  });

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
    $scope.user = {};
  });

  function setBuscarSexo(){
    const d = $q.defer();

    //alert(JSON.stringify($scope.user.buscar.sexo));
    $scope.user.buscar.sexo_m = $scope.sexos[0].selected ? false : true;
    $scope.user.buscar.sexo_f = $scope.sexos[1].selected ? false : true;
    
    if(typeof($scope.user.buscar.sexo) !== 'undefined' || $scope.user.buscar.sexo !== []){
      var sexo = [
        {
          nome: 'Homem', 
          selected: $scope.sexos[0].selected ? false : true
        },{
          nome: 'Mulher',
          selected: $scope.sexos[1].selected ? false : true
        }
      ];
    }else{
      var sexo = [];
    }

    d.resolve(sexo);
  }

  $scope.checarSexo = function(opt){
    angular.forEach($scope.sexos, function(value, key) {
      if(value.nome != opt){
        value.selected = false;
      }else{
        if(value.selected == true){
          $scope.user.sexo = value.nome;
          $scope.ok.disabled = false;
        }else{
          $scope.ok.disabled = true;
        }
      }
    });
  }

  $scope.goNext = function(){
    //alert(JSON.stringify($scope.user));

    setBuscarSexo().then(function(r){
      //alert(r);
      $scope.user.buscar.sexo = r;
      UserService.setUser($scope.user);
      $state.go('tab.sel-escolaridade');  
    });
  }
})

.controller('SelSexoCtrl',function($rootScope, $state, $scope, UserService, $q){

  $scope.ok = {};

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    $scope.ok.disabled = true;

    //alert($scope.user.sexo);
    //alert('BUSCAR ' + JSON.stringify($scope.user.buscar));

    if(typeof($scope.user.sexo) === 'undefined' || $scope.user.sexo === null){
      $scope.sexos = [
        { 
          nome: 'Homem',
          selected: false
        },
        { 
          nome: 'Mulher',
          selected: false
        }
      ];
      $scope.ok.disabled = true;
    }else{
      $scope.sexos = [
        { 
          nome: 'Homem',
          selected: $scope.user.sexo == 'Homem' ? true : false
        },
        { 
          nome: 'Mulher',
          selected: $scope.user.sexo == 'Mulher' ? true : false
        }
      ];
      $scope.ok.disabled = false;
      $scope.user.buscar.sexo = [
        {
          nome: 'Homem', 
          selected: $scope.sexos[0].selected ? false : true
        },{
          nome: 'Mulher',
          selected: $scope.sexos[1].selected ? false : true
        }
      ];
    }
  });

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
    $scope.user = {};
  });

  function setBuscarSexo(){
    const d = $q.defer();

    //alert(JSON.stringify($scope.user.buscar));

    try{
      $scope.user.buscar.sexo_m = $scope.sexos[0].selected ? false : true;
      $scope.user.buscar.sexo_f = $scope.sexos[1].selected ? false : true;
      d.resolve();
    }catch(e){
      d.reject(e);
    }
    //alert(typeof($scope.user.buscar.sexo));

    // try{
    //   if(typeof($scope.user.buscar.sexo) !== 'undefined' || $scope.user.buscar.sexo !== []){
    //     var sexo = [
    //       {
    //         nome: 'Homem', 
    //         selected: $scope.sexos[0].selected ? false : true
    //       },{
    //         nome: 'Mulher',
    //         selected: $scope.sexos[1].selected ? false : true
    //       }
    //     ];
    //     d.resolve(sexo);
    //   }else{
    //     d.resolve('e');
    //   }
    // }catch(e){
    //   d.reject(e);
    // }
  }

  $scope.checarSexo = function(opt){
    angular.forEach($scope.sexos, function(value, key) {
      if(value.nome != opt){
        value.selected = false;
      }else{
        if(value.selected == true){
          $scope.user.sexo = value.nome;
          $scope.ok.disabled = false;
        }else{
          $scope.ok.disabled = true;
        }
      }
    });
  }

  $scope.goNext = function(){
    try {
      setBuscarSexo();
    }
    // catch(e){
    //alert(e)
    // }
    finally {
      UserService.setUser($scope.user);
      $state.go('tab.sel-escolaridade');  
    };
  }
})

.controller('SelEscolaridadeCtrl', function($state, $scope, UserService){

  $scope.ok = {};
  $scope.user = UserService.getUser();

  $scope.escolaridade = [
    {
      nome: 'Fundamental',
      selected: false
    },
    {
      nome: 'Médio',
      selected: false
    },
    {
      nome: 'Superior',
      selected: false
    }
  ];

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    //alert(JSON.stringify($scope.user));
    $scope.ok.disabled = true;
    
    if(typeof($scope.user.escolaridade) !== 'undefined' && $scope.user.escolaridade !== null){
      $scope.ok.disabled = false;
    }
  });

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
    $scope.user = {};
  });
  
  $scope.setEscolaridade = function(opt){
    angular.forEach($scope.escolaridade, function(value, key) {
      if(value.nome != opt){
        value.selected = false;
      }else {
        if(value.selected == true){
          $scope.user.escolaridade = value.nome;
          $scope.ok.disabled = false;
        }else{
          $scope.ok.disabled = true;
        }
      }
    });
  }

  $scope.goNext = function(){
    UserService.setUser($scope.user);
    $state.go('tab.sel-estilos');
  }
})

.controller('SelEstilosCtrl', function($scope, $state, UserService, Ritmos){
  $scope.ok = {};
  
  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    //alert(JSON.stringify($scope.user));
    $scope.ok.disabled = true;
    $scope.ritmos = Ritmos;

    if(typeof($scope.user.ritmos) == 'undefined' || $scope.user.ritmos == null || $scope.user.ritmos == []){
      angular.forEach($scope.ritmos, function(value, key) {
        value.checked = false;
      });
      $scope.user.ritmos = [];
    }else{
      $scope.ok.disabled = false;
    }
  });

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
    $scope.user = {};
    $scope.ritmos = Ritmos;
  });

  $scope.selectRit = function(r){
    var sel = false;
    
    angular.forEach($scope.ritmos, function(value, key) {
      if(r.id == value.id){
        value.checked = !value.checked;
        if(value.checked == true){
          r.checked = true;
          $scope.user.ritmos.push(value.nome);
          $scope.ok.disabled = false;
          sel = true;
        }else{
          var index = $scope.user.ritmos.indexOf(value.nome);
          if (index > -1) {
            $scope.user.ritmos.splice(index, 1);
          }
        }
      }
    });

    if(sel == false){
      $scope.ok.disabled = true;
    }
  }

  $scope.goNivel = function(){
    UserService.setUser($scope.user);
    $state.go('tab.sel-nivel')
  }
})

.controller('SelNivelCtrl', function($scope, $state, UserService, $http, Url, database, $cordovaSQLite, $ionicPlatform){

  $scope.ok = {};
  
  $scope.niveis =  [
    {
      nome: 'Quero Aprender',
      selected: false
    }, 
    {
      nome: 'Básico',
      selected:false
    }, 
    {
      nome: 'Intermediário',
      selected:false
    }, 
    {
      nome: 'Avançado',
      selected: false
    }];

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    //alert(JSON.stringify($scope.user));
    if(typeof($scope.user.nivel) == 'undefined' || $scope.user.nivel == null){
      $scope.ok.disabled = true;
    }else{
      $scope.ok.disabled = false;
    }
  });

  $scope.$on('$ionicView.leave', function(){
    $scope.user = {};
  });

  $scope.setNivel = function(opt){
    angular.forEach($scope.niveis, function(value, key) {
      if(value.nome != opt){
        value.selected = false;
      }else{
        if(value.selected == true){
          $scope.user.nivel = value.nome;
          $scope.ok.disabled = false;
        }else{
          $scope.ok.disabled = true;
          $scope.user.nivel = '';
        }
      }
    });
  }

  $scope.goCards = function(){
    UserService.setUser($scope.user);
    $http.post(Url.url + '/api/saveUserPost', {user: angular.toJson($scope.user)}).then(function(res){
      if(res.data.user){
        const _user = res.data.user;

        database.update('user', 'id', _user.id, _user).then(function(res){
          $state.go('tab.cards');
        }, function(err){
          //alert(JSON.stringify(err));
        });
      }
    });
  }
})

.controller('PerfilCtrl',function($state, $scope, database, UserService){
  $scope.$on('$ionicView.loaded', function(){
    // select only one field to use less memory
    database.select('user').then(function(r){
      $scope.user = r.item(0);
      $scope.user.foto = r.item(0).fotos.split(',')[0];
      $scope.user.fotos = r.item(0).fotos.split(',');
      UserService.setUser($scope.user);
      //alert($scope.user.foto);
    });
  });
})

.controller('ConfigCtrl',function(database, $filter, $http, $ionicScrollDelegate, $ionicHistory, $ionicLoading, $ionicPopup, $q, $rootScope, $scope, $state, Url, UserService){
  $scope.slider = {
    raio: 35,
    min: 18,
    max: 55
  };

  $scope.ageSlider = {
    options: {
      floor: 18,
      ceil: 55,
      hidePointerLabels: true,
    }
  };

  $scope.distSlider = {
    options: {
      floor: 1,
      ceil: 100,
      hidePointerLabels: true,
      showSelectionBar: true
    }
  };

  $scope.hide = {};

  $scope.config = [
    {
      nome: 'Matches',
      selected: true
    },
    {
      nome: 'Mensagens',
      selected: true
    }
  ];

  $scope.sexos = [
    {
      nome: 'Mulheres',
      selected: true
    },
    {
      nome: 'Homens',
      selected: true
    }
  ];

  $scope.niveis = [
    {
      nome: 'Quero Aprender',
      selected: false
    },
    {
      nome: 'Básico',
      selected: false
    },
    {
      nome: 'Intermediário',
      selected: false
    },
    {
      nome: 'Avançado',
      selected: false
    }
  ];

  $scope.goRitmosS = function(){
    // UserService.setUser($scope.user);
    $state.go('tab.ritmos-s');
  }

  $scope.goPerfil = function(){
    //UserService.setUser($scope.user);
    $state.go('tab.perfil');
  }

  $scope.goCards = function(){
    //UserService.setUser($scope.user);
    $state.go('tab.cards');
  }

  $scope.goChats = function(){
    UserService.setUser($scope.user);
    $state.go('tab.chats');
  }

  $scope.$on('$ionicView.enter', function(){
    database.select('user').then(function(r){
      $scope.user = r.item(0);

      // alert(JSON.stringify($scope.user));

      database.select('buscar').then(function(res){
        // alert('BUSCAR RES');

        $scope.user.buscar = res.item(0);
        
        // alert(JSON.stringify($scope.user.buscar));
        //alert(res.item(0).ritmos.split(','));

        $scope.user.buscar.ritmos = res.item(0).ritmos.split(',');
        //$scope.user.buscar.ritmos = res.item(0).ritmos.split(',');

        //alert(JSON.stringify($scope.user));

        $ionicScrollDelegate.scrollTop();

        $scope.slider = {
          raio: $scope.user.raio,
          min: res.item(0).age_min,
          max: res.item(0).age_max
        };

        //alert(JSON.stringify(res.item(0)));

        // if(typeof($scope.user.buscar.sexo) !== 'undefined'){
        //   $scope.sexos = $scope.user.buscar.sexo;
        // }else{
        //   $scope.sexos = [
        //     {
        //       nome: 'Mulheres',
        //       selected: true
        //     },
        //     {
        //       nome: 'Homens',
        //       selected: true
        //     }
        //   ];
        // }
      
        // if(typeof($scope.user.buscar.config) !== 'undefined'){
        //   $scope.config = $scope.user.buscar.config;
        // }else{
        //   $scope.config = [
        //     {
        //       nome: 'Matches',
        //       selected: true
        //     },
        //     {
        //       nome: 'Mensagens',
        //       selected: true
        //     }
        //   ];
        // }

        // alert(res.item(0).niveis);

        // alert('TYPEOF NIVEIS UNDEFINED');
        // alert(typeof(res.item(0).niveis) == 'undefined');
        // alert(res.item(0).niveis == "null");

        // if(typeof(res.item(0).niveis) == 'undefined' || res.item(0).niveis == "null"){
        // //   //$scope.niveis = $scope.user.buscar.niveis;
          
        // // }else{
        //   //alert('TRUEE');
        var _niveis = res.item(0).niveis == "null" ? ['Básico'] : res.item(0).niveis.split(',');
        // }
        _niveis.forEach(function(item){
          $scope.setUserNiveis(item, true, true);
        })
        
        //alert(JSON.stringify($scope.user.buscar.niveis));

        // setInterval(function() {
        //   $scope.$apply();
        //   $scope.$digest();
        //   //$ionicLoading.hide();
        // }, 1000);
      });
    });
  });

  $scope.selSexo = function(opt){
    angular.forEach($scope.sexos, function(value, key) {
      if(value.nome == opt){
        if(value.selected == false){
          if(key == 0){
            $scope.sexos[1].selected = true;
            $scope.user.buscar.sexo_f = true;
          }else{
            $scope.sexos[0].selected = true;
            $scope.user.buscar.sexo_m = true;
          }
        }
      }
    });
    //alert(JSON.stringify($scope.sexos));
  }

  $scope.selNotificacoes = function(opt){
    angular.forEach($scope.config, function(value, key) {
      if(value.nome == opt){
        if(opt == "Matches"){
          $scope.config[0].selected = value.selected;
          $scope.user.buscar.noti_match = value.selected;
        }
        if(opt == "Mensagens"){
          $scope.config[1].selected = value.selected;
          $scope.user.buscar.noti_msg = value.selected;
        }
      }
    });
  }

  // $scope.ver = function(){
  // alert(JSON.stringify($scope.niveis));
  // alert(JSON.stringify($scope.user.buscar.niveis));
  // }


  function showAlert() {
    $ionicPopup.alert({
     title: 'Dados atualizados',
     template: 'Suas modificações foram salvas'
    });
  };

  $scope.salvar = function(){
    //alert(JSON.stringify($scope.user));

    $scope.user.raio = $scope.slider.raio;

    //alert(JSON.stringify($scope.slider));

    $scope.user.buscar.age_min = $scope.slider.min;
    $scope.user.buscar.age_max = $scope.slider.max;

    //alert($scope.user.buscar);

    database.select('buscar').then(function(r){
      if(r.length > 0){
        var oldBuscar = r.item(0);
        //alert('buscar res');

        var user = angular.copy($scope.user);
        // alert(JSON.stringify(user));

        // alert($scope.user.buscar.niveis);

        // alert(angular.isArray($scope.user.buscar.niveis));

        user.buscar.niveis = angular.isArray($scope.user.buscar.niveis) ? $scope.user.buscar.niveis.join(',') : $scope.user.buscar.niveis;

        // alert(JSON.stringify(user.buscar));

        // alert(JSON.stringify(oldBuscar));

        // //alert(JSON.stringify(user));
        // alert('equals');

        // alert(angular.equals(oldBuscar, user.buscar));

        if(!angular.equals(oldBuscar, user.buscar)){
          //alert(JSON.stringify(user));
          // alert('saveUserPost');
          // alert(JSON.stringify(user));
          $http.post(Url.url + '/api/saveUserPost',{user: angular.toJson(user)}).then(function(res){
            database.update('user', 'id', user.id, user).then(function(r){
              database.update('buscar', 'user_id', user.buscar.user_id, user.buscar).then(function(r){
                showAlert();
              }
              // , function(err){
              //   //alert('err db update buscar ' + JSON.stringify(err));
              // }
              );
            }
            // , function(err){
            // alert('err db update user ' + JSON.stringify(err));
            // }
            );
          }
          // , function(err){
          //alert('err post salvar ' + JSON.stringify(err));
          // }
          );
        }
      }
    });
  }

  $scope.setUserNiveis = function(nome, selected, first){
    //alert('setUserNiveis');

    if(typeof($scope.user.buscar.niveis) == "string"){
      var slice = $scope.user.buscar.niveis.split(',');
      $scope.user.buscar.niveis = slice;
    }

    // alert('niveis');
    // alert(JSON.stringify($scope.niveis));

    angular.forEach($scope.niveis, function(value, key) {
      if(value.nome == nome){

        var index = $scope.user.buscar.niveis.indexOf(value.nome);
        // alert(index);

        if(!first){
          if (index > -1) {
            $scope.user.buscar.niveis.splice(index, 1);
          }else{
            $scope.user.buscar.niveis.push(value.nome);
          }
        }

        value.selected = selected;
        // alert(!value.selected);
        // $scope.$apply();
        // alert(JSON.stringify(value));

      }
    });

    // $scope.niveis.map(function(item){
    //   return item;
    // })

    // $timeout(function(){
    //   UserService.setUser($scope.user);
    //   // alert('niveis');
    //   // alert(JSON.stringify($scope.niveis));
    //   // alert(JSON.stringify($scope.user.buscar.niveis));
    // }, 3000);
  }

  // $scope.$on('$ionicView.leave', function(){
  //   $scope.user.buscar.age_min = $scope.slider.min;
  //   $scope.user.buscar.age_max = $scope.slider.max;
  //   $scope.user.buscar.config = $scope.config;
  //   $scope.user.buscar.niveis = $scope.niveis;
  //   $scope.user.raio = $scope.slider.raio;

  //   //UserService.setUser($scope.user);

  //   //$scope.user = {};
  // });

  function popupShow(){
    var showPopup = $ionicPopup.show({
      template: 'Você será desconectado.',
      title: 'Tem Certeza?',
      cssClass: 'text-center',
      scope: $scope,
      buttons: [
        { 
          text: 'SIM',
          type: 'button-positive',
          onTap: function(r){
            return 0;
          }
        },
        {
          text: 'NÃO',
          type: 'button-positive',
          onTap: function(r){
            return 1;
          }
        }
      ]
    });

    showPopup.then(function(res){
      if(res == 0){
        
        // UserService.empty();
        
        // database.deleteDB().then(function(r){
        //alert('database dfelete r ' + JSON.stringify(r));

        //   if($scope.user.social_network == 'facebook'){
        //     facebookConnectPlugin.logout();
        //   }else{
        //     window.plugins.googleplus.logout();
        //     window.plugins.googleplus.disconnect();
        //   }

        //   $state.go('tab.login');
        // }, function(err){
        //alert('err ' + JSON.stringify(err));
        // });

        try{
          window.plugins.OneSignal.logoutEmail($scope.user.email);
          // window.plugins.OneSignal.logoutEmail(user.email);
        
          if($scope.user.social_network == 'facebook'){
            facebookConnectPlugin.logout();
          }else{
            window.plugins.googleplus.logout();
            window.plugins.googleplus.disconnect();
          }

          database.dropTable('user')
            // .then(function(r){
            //   //alert(JSON.stringify(r));
            // }, function(err){
            //   //alert(JSON.stringify(err));
            // })
            .then(function(){
              database.dropTable('buscar').then(function(r){
                //alert(JSON.stringify(r));
                database.deleteDB();
              }
              // , function(err){
              //alert(JSON.stringify(err));
              // }
              );
            });
          
          $ionicHistory.clearCache();
          $ionicHistory.clearHistory();
        }
        // catch(e){
        //alert(JSON.stringify(e));
        // }
        finally{
          database.select('user')
            // .then(function(r){
            //alert(JSON.stringify(r))
            // }, function(err){
            //alert(JSON.stringify(err))
            // })
          //alert('finally');
          // $scope.user = {};
          // UserService.empty();
          $state.go('tab.login');
        }

      }
    });
  }

  $scope.sair = function(){
    popupShow();
  }

  $scope.delete = function(){
    var user = UserService.getUser();

    $http.post(Url.url + '/api/delUser', {user: angular.toJson(user)}).then(function(res){
      if(res.status == 200){
        try{
          window.plugins.OneSignal.logoutEmail(user.email);
        
          if($scope.user.social_network == 'facebook'){
            facebookConnectPlugin.logout();
          }else{
            window.plugins.googleplus.logout();
            window.plugins.googleplus.disconnect();
          }

          database.dropTable('user')
          // .then(function(r){
          //   //alert(JSON.stringify(r));
          // })
          .then(function(){
            database.dropTable('buscar').then(function(r){
              //alert(JSON.stringify(r));
              database.deleteDB();
            });
          })

          $ionicHistory.clearCache();
          $ionicHistory.clearHistory();
        }
        // catch(e){
        //alert(JSON.stringify(e))
        // }
        finally{
          $scope.user = {};
          UserService.empty();
          $state.go('tab.login');
        }
      }
    });
  }

  $scope.popupShowDelete = function(){
    var showPopupDel = $ionicPopup.show({
      template: 'Seus dados serão excluídos.',
      title: 'Tem Certeza?',
      cssClass: 'text-center',
      scope: $scope,
      buttons: [
        { 
          text: 'SIM',
          type: 'button-positive',
          onTap: function(r){
            return 0;
          }
        },
        {
          text: 'NÃO',
          type: 'button-positive',
          onTap: function(r){
            return 1;
          }
        }
      ]
    });

    showPopupDel.then(function(res){
      if(res == 0){
        $scope.delete();
      }
    });
  }
})

.controller('RitmosSCtrl',function($state, $scope, $http, UserService, $q, Ritmos, Url, $ionicHistory, database){
  $scope.$on('$ionicView.beforeEnter', function(){
    database.select('buscar').then(function(r){
      $scope.buscar = r.item(0);
      $scope.buscar.ritmos = r.item(0).ritmos == "null" ? [] : r.item(0).ritmos;
      setRitmos();
    }
    // , function(err){
    //   //alert(JSON.stringify(err));
    // }
    );
  });

  function setRitmos(){
    //alert(JSON.stringify($scope.buscar.ritmos));
    try{
      $scope.ritmos = Ritmos;
    }finally{
      if($scope.buscar.ritmos == '' || $scope.buscar.ritmos == 'null' || $scope.buscar.ritmos == null){
        //alert('if');
        $scope.buscar.ritmos = [];
      }else{
        //alert('else');
        $scope.buscar.ritmos = $scope.buscar.ritmos.split(',');
        $scope.ritmos.map(function(value, key) {
          try{
            var index = $scope.buscar.ritmos.indexOf(value.nome);
            if (index > -1) {
              value.checked = true;
            }else{
              value.checked = false;
            }
          }finally{
            return value;
          }
        });
      }
    }
  }

  $scope.selectRit = function(r){
    //alert(JSON.stringify($scope.buscar.ritmos));
    angular.forEach($scope.ritmos, function(value, key) {
      try{
        var index = $scope.buscar.ritmos.indexOf(value.nome);
        if(value.nome == r.nome){
          value.checked = !value.checked;
          if (index > -1) {
            $scope.buscar.ritmos.splice(index, 1);
          }else{
            $scope.buscar.ritmos.push(value.nome);
            //alert(JSON.stringify($scope.buscar.ritmos));
          }
        }
      }
      finally{
        return value;
      }
    });
  }

  $scope.goBack = function(){
    //alert(JSON.stringify($scope.buscar.ritmos));
    try{
      saveBuscar()
    }
    // catch(e){
    //alert(JSON.stringify(e))
    // }
    finally{
      $state.go('tab.config');
    }
  }


  function saveBuscar(){
    $scope.buscar.ritmos = $scope.buscar.ritmos.join(',');
    database.update('buscar', 'user_id', $scope.buscar.user_id, $scope.buscar)
      // .then(function(r){
      //alert(JSON.stringify(r))
      // }, function(err){
      //alert(JSON.stringify(err))
      // })
  }
})

.controller('EditarCtrl',function(database, $cordovaCamera, $cordovaFileTransfer, $http, $ionicActionSheet, $ionicHistory, $ionicLoading,  $ionicScrollDelegate, $q, $rootScope, $scope, $state, Url, UserService){

  $scope.chrLength = 0;
  $scope.updated = 0;

  $scope.sexos = [
    { 
      nome: 'Homem',
      selected: false
    },
    { 
      nome: 'Mulher',
      selected: false
    }
  ];

  $scope.niveis = [
    {nome: 'Quero Aprender', selected: false},
    {nome: 'Básico', selected: false},
    {nome: 'Intermediário', selected: false},
    {nome: 'Avançado', selected: false}
  ];

  $scope.escolaridade = [
    {
      nome: 'Fundamental',
      selected: false, 
    },
    {
      nome: 'Médio',
      selected: false
    },
    {
      nome: 'Superior',
      selected: false
    }
  ];

  $scope.$on('$ionicView.enter', function(){
    database.select('user').then(function(res){
      if(res.length > 0){
        $scope.user = res.item(0);

        $scope.user.about = $scope.user.about == 'null' ? '' : $scope.user.about;
        $scope.user.work = $scope.user.work == 'null' ? '' : $scope.user.work;
        $scope.user.ritmos = res.item(0).ritmos.split(',');

        //alert(JSON.stringify($scope.user.ritmos));

        $scope.user.fotos = res.item(0).fotos.split(',');
        $ionicScrollDelegate.scrollTop();

        $scope.selNivel($scope.user.nivel);
        $scope.checarEscolaridade($scope.user.escolaridade);
        $scope.checarSexo($scope.user.sexo);
        
        database.select('buscar').then(function(r){
          if(r.length > 0){
            $scope.user.buscar = r.item(0);
            UserService.setUser($scope.user);
          }
        });
      }
    }
    // , function(err){
    //     //alert(JSON.stringify(err))
    // }
    );
  });

  $scope.checarSexo = function(opt){
    angular.forEach($scope.sexos, function(value, key) {
      if(value.nome != opt){
        value.selected = false;
      }else{
        $scope.user.sexo = value.nome;
        value.selected = true;
      }
    });
  }

  $scope.checarEscolaridade = function(opt){
    angular.forEach($scope.escolaridade, function(value, key) {
      if(value.nome != opt){
        value.selected = false;
      }else{
        $scope.user.escolaridade = value.nome;
        value.selected = true;
      }
    });
  }

  $scope.selNivel = function(opt){
    angular.forEach($scope.niveis, function(value, key) {
      if(value.nome != opt){
        value.selected = false;
      }else{
        $scope.user.nivel = value.nome;
        value.selected = true;
      }
    });
  }

  $scope.$watch('user.about', function(newValue, oldValue){
    if (newValue.length > 120){
      $scope.user.about = oldValue;
    }
  });

  function diff_years(dt2, dt1){
    var diff =(dt2.getTime() - dt1.getTime()) / 1000;
     diff /= (60 * 60 * 24);
    return Math.abs(Math.round(diff/365.25));
  }

  $scope.verifyAge = function(){
    if(diff_years(new Date(), new Date($scope.user.birthday)) < 18){
      $rootScope.showAlert('Data Inválida','Apenas maiores de 18 anos podem utilizar o Bailaki.');
      $scope.user.birthday = '';
    }
  };

  function onSuccess(res){
    var img = res.toURL();
    var pic = {img: img, class: 'rm'};
    var k = 0;
    for(var i = 0; i < $scope.user.fotos.length; i++){
      if($scope.user.fotos[i].img == 'img/placeholder.png'){
        $scope.user.fotos[i] = pic;
        k = i;
        break;
      }
    }
    upload_foto($scope.user, img, k);
    $scope.$apply();
  }

  function moveFile(fileUri) {
    window.resolveLocalFileSystemURL(
      fileUri,
      function(fileEntry){
        window.resolveLocalFileSystemURL(
          cordova.file.dataDirectory,
          function(dirEntry) {
            var newFilename = 'b-' + Date.parse(new Date()).toString() + '.jpg';
            fileEntry.moveTo(dirEntry, newFilename, function(res){
              onSuccess(res);
            }/*,
            function(err){
              onFail('moveTo err',err);
            }*/
            );
          }
          /*,
          function(err){
            onFail('resolveLocalFileSystemURL folder err',err);
          }
          */
          );
      }
      /*,
      function(err){
        onFail('resolveLocalFileSystemURL file Err',err);
      } */
      );
  }

  $scope.openFoto = function(i){
    if($scope.user.fotos.length > 1 && $scope.user.fotos[i]){
      $scope.user.fotos.splice(i, 1);
    }else{
      escolherAlbum(i);
    }
  }

  function escolherAlbum(i){
    if($scope.user.social_network == 'facebook'){
      $ionicActionSheet.show({
          buttons: [
           { text: 'Facebook' },
           { text: 'Aparelho' }
          ],
          titleText: 'Carregar foto do',
          cancelText: 'Cancelar',
          buttonClicked: function(index) {
            if(index == 0){
              albums_func(i);
              return true;
            }else{
              abrirGaleria(i);
              return true;
            }
          }
        });
    }else{
      abrirGaleria(i);
    }
  }

  function abrirGaleria(i){
    var optionsC = {
      quality: 100,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    $cordovaCamera.getPicture(optionsC).then(function(imagePath){
      upload_foto(imagePath, i);
    });
  }

  var upload_foto = function(path, i){
    //alert('upload_foto');

    var filename = path.split('/').pop();
    //alert(filename);

    const dados = {
      user: $scope.user.id,
      token: $scope.user.token
    };

    var opts = {};
    opts.fileKey = 'file';
    opts.fileName = filename;
    opts.chunkedMode = false;
    opts.mimeType = 'image/jpg';
    opts.httpMethod = 'POST';
    opts.params = {data: dados}

    var j;
    if(i == 0){
      j = 0;
    }else{
      j = $scope.user.fotos.length;
    }

    $scope.user.fotos[j] = path;
    
    UserService.setUser($scope.user);

    var user = angular.copy($scope.user);
    
    $ionicLoading.show();

    $cordovaFileTransfer.upload(Url.url + '/api/fileUpload', path, opts, true).then(function(res){
      var r = JSON.parse(res.response);
      //alert(JSON.stringify(r));
      if(r[0] == 1){
        user.fotos[j] = r[1];
        UserService.setUser(user);
        $scope.user = user;
        
        $http.post(Url.url + '/api/saveUserPost',{user: angular.toJson(user)})
        // .then(function(res){
        //   //alert(JSON.stringify(res));
        // }
        // // , function(err){
        // //alert(JSON.stringify(err));
        // // }
        // );
      }
      $ionicLoading.hide();
    }, function(err){
      //alert(JSON.stringify(err));
      $ionicLoading.hide();
    });
  }

  $scope.salvar = function(){
    var oldUser = angular.copy(UserService.getUser());
    UserService.setUser($scope.user);

    if(angular.equals($scope.user, oldUser)){
      // $http.post(Url.url + '/api/saveUserPost',{user: angular.toJson($scope.user)}).then(function(res){});
      var _fotos = $scope.user.fotos.join(',');
      var _user = angular.copy($scope.user);
      _user.fotos = _fotos;

      database.update('user', 'id', _user.id, _user).then(function(r){
        $http.post(Url.url + '/api/saveUserPost',{user: angular.toJson(_user)}).then(function(res){
          $rootScope.showAlert('Dados atualizados', '');
        });
      }
      // , function(err){
      //alert(JSON.stringify(err));
      // }
      );
    }
  }

  var albums_func = function(i){
    $rootScope.albums = [];
    facebookConnectPlugin.api('/me?fields=albums{cover_photo{picture}}&access_token=' + $scope.user.accessToken, [],
      function (response) {
        if(response.albums){
          $rootScope.albums = response.albums.data;
          $rootScope.foto_ = i;
          $ionicLoading.hide();
          $state.go('tab.albums');
        }
        else{
          facebookConnectPlugin.api('/' + response.id + '/albums?fields=cover_photo{picture}', [], function(res){
            $rootScope.albums = response.albums.data;
          });
        }
      });
  }

  var downloadFile = function(user, url, g_i){    
    $rootScope.checkPermission();

    $http.get(user.fotos[0].img + '&redirect=false').then(function(res){

      var targetPath = cordova.file.externalRootDirectory + '/bailaki/profile-bailaki.png';

      $cordovaFileTransfer.download(res.data.data.url, targetPath, {}, true)
        .then(function(result) {
          upload_foto(user, targetPath, 0);
        }, function(error) {
          $rootScope.showAlert('Erro ao configurar a foto.');
        });
    }
    );
  }

  $scope.goRitmos = function(){
    $state.go('tab.ritmos');
  }
})

.controller('RitmosCtrl',function(database, $ionicScrollDelegate, $state, $scope, $http, UserService, $q, Ritmos, Url, $ionicHistory, $timeout){
  $scope.$on('$ionicView.enter', function(){
    // $scope.user = UserService.getUser();
    database.select('user').then(function(res){
      if(res.length > 0){
        $scope.user = res.item(0);

        //alert('user ritmos ' + JSON.stringify($scope.user.ritmos));
        // alert('type user ritmos ' + typeof($scope.user.ritmos));

        if($scope.user.ritmos.length > 0 &&  $scope.user.ritmos !== 'null'){
          $scope.user.ritmos = $scope.user.ritmos.split(',');
        }else{
          $scope.user.ritmos = [];
        }

        setRitmos();
        $ionicScrollDelegate.scrollTop();
      }
    });
  });

  $scope.selectRit = function(r, event){
    // alert(JSON.stringify($scope.user.ritmos));
    // event.preventDefault();
    // event.stopPropagation();
    // alert(JSON.stringify(event));

    // var index = $scope.user.ritmos.indexOf(r.nome);

    // alert(index);

    // angular.forEach($scope.ritmos, function(value, key) {
    //   if(value.nome == r.nome){
    //     // value.checked = !value.checked;

    //     if (index > -1) {
    //       $scope.user.ritmos.splice(index, 1);
    //     }else{
    //       $scope.user.ritmos.push(value.nome);
    //     }
    //   }
    // });

    angular.forEach($scope.ritmos, function(value, key) {
      //try{
        var index = $scope.user.ritmos.indexOf(value.nome);

        if(value.nome == r.nome){
          value.checked = !r.checked;
          
          if (index > -1) {
            $scope.user.ritmos.splice(index, 1);
          }else{
            $scope.user.ritmos.push(r.nome);
          }
        }
      //}
      // finally{
      //   return value;
      // }
    });

    // // 
    // $timeout(function(){
    //   //alert('after ' + JSON.stringify($scope.user.ritmos));
    // }, 4000);
  }

  function setRitmos(){
    $scope.ritmos = Ritmos;
    angular.forEach($scope.ritmos, function(value, key) {
      var index = $scope.user.ritmos.indexOf(value.nome);
      if (index > -1) {
        value.checked = true;
      }else{
        value.checked = false;
      }
    });
  }

  $scope.goBack = function(){
    //alert('LEAVE');
    saveUserRitmos();
  }

  function saveUserRitmos(){
    var user = angular.copy($scope.user);
    user.ritmos = $scope.user.ritmos.join(',');
    //alert(user.ritmos);
    database.update('user', 'id', user.id, user).then(function(r){
      //alert(JSON.stringify(r));
      $state.go('tab.editar');
    }
    // , function(err){
    //alert(JSON.stringify(err));
    // }
    );
  }

  $scope.$on('$ionicView.leave', function(){
    saveUserRitmos();
  });

})

.controller('AlbumsCtrl', function($state, $scope, UserService, $rootScope){
  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    $scope.albums = $rootScope.albums;
  });

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
  });

  $scope.goAlbum = function(id){
    $rootScope.alb_id = id;
    $state.go('tab.fotos');
  }
})

.controller('FotosCtrl',function(database, $cordovaFileTransfer, $http, $ionicHistory, $ionicScrollDelegate, $rootScope, $scope, $state, Url, UserService){

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    //alert('user ' + JSON.stringify($scope.user));

    $scope.fotos = [];
    $scope.url = '/'+$rootScope.alb_id+'/photos?fields=picture,images&access_token='+$scope.user.accessToken;
    loadPhotos(0,$scope.url);
  });

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
  });

  $scope.scroll = function(){
    var currentTop = $ionicScrollDelegate.getScrollPosition().top;
    var maxTop = $ionicScrollDelegate.getScrollView().__maxScrollTop;
    if (currentTop >= maxTop && $scope.url.length > 0) {
      loadPhotos(0, $scope.url);
    }
  }

  function loadPhotos(i, url){
    facebookConnectPlugin.api(url, null,
      function(response){
        $scope.$apply(function(){
          var fotos = $scope.fotos.concat(response.data);
          $scope.fotos = fotos;
          $ionicScrollDelegate.resize();

          if(response.paging.next){
            var alb = response.paging.next.split($rootScope.alb_id);
            i++;
            $scope.url =  '/' + $rootScope.alb_id + alb[1];
            if(i < 2){
              loadPhotos(i, $scope.url);
            }
          }else{
            $scope.url = '';
          }
        });
      });
  }

  $scope.selectPiture = function(picture){
    //alert('selectPiture')

    //alert(typeof($scope.user.fotos));
    //alert(JSON.stringify($scope.user.fotos));

    //try{
      if(typeof($scope.user.fotos) == 'string'){
        var fotos = $scope.user.fotos.split(',');
      }else{
        var fotos = $scope.user.fotos; //.split(',');
      }

      //alert('fotos ' + JSON.stringify(fotos));
      //alert('picture ' + JSON.stringify(picture));

      if(fotos.length < 3){
        const len = fotos.length == 1 ? 0 : fotos.length;
        
        //alert(len);

        var data = {};
        data.facebook = true;
        data.url = picture.images[0].source;
        data.user = $scope.user.id;
        data.token = $scope.user.token;
        data.i = len;

        //alert('data ' + JSON.stringify(data))

        $http.post(Url.url + '/api/fileUpload',{data: angular.toJson(data)}).then(function(res){
          const res_data = res.data.data;
          //alert('res data ' + JSON.stringify(res_data));
          if(res_data[0] == 1){
            fotos.push(res_data[1]);
            
            //alert(JSON.stringify(fotos));

            $scope.user.fotos = fotos;

            UserService.setUser($scope.user);
            var fotos_ = fotos.join(',');
            var user_ = angular.copy($scope.user);
            user_.fotos = fotos_;

            //alert(fotos_);

            $http.post(Url.url + '/api/saveUserPost', {user:  angular.toJson(user_)}).then(function(res){
              if(res.status == 200){
                database.update('user', 'id', user_.id, user_).then(function(r){
                  $state.go('tab.editar');
                }
                // , function(err){
                //alert(JSON.stringify(err));
                // }
                );
              }
            }
            // , function(err){
            //alert('ERR SALVAR ' + JSON.stringify(err))
            // }
            );
          }
          // else{
          //alert('res data else ' + JSON.stringify(res_data));
          // }
        }
        // , function(err){
        //alert('upload err ' + JSON.stringify(err));
        // }
        );
      }
    //}
    // catch(e){
    //alert('e ' + JSON.stringify(e));
    // }
  }
})

.controller('CardsCtrl', function($cordovaGeolocation, $http, $ionicHistory, $ionicLoading, $ionicModal, $ionicPopup, $q, $rootScope, $scope, $state, $timeout, Url, UserService, database) { // socket,
  $scope.hide = {};
  
  $scope.url = 'https://bailaki.com/imagens_bailaki/uploads';
  $scope.cards = [];

  $scope.$on('$ionicView.leave', function(){
    //alert('Cards Leave');
    // $scope.user = {};
    // UserService.empty();
    $rootScope.inCards = false;
  });
  
  $scope.$on('$ionicView.loaded', function(){
    $scope.hide.ctrl = true;
    $scope.hide.notice = true;
    //alert('Loaded Enter Cards');
  });


  function entrarChats(match){
    for(var i = 0; i < match.length; i++){
      socket.emit('userOpened', match[i].chatId, $scope.user.id);
    }
  }


  // function setSocket(){
  //   socket.on('connect_error', err => handleErrors(err));
  //   socket.on('connect_failed', err => handleErrors(err));
  //   socket.on('disconnect', err => handleErrors(err));

  //   socket.on('match', function(matchId, userId, nome){
  //     $rootScope.match = {matchId: matchId, id: userId, nome: nome};
  //     $state.go('tab.match');
  //   });

  //   socket.on('message', function(a, b){
  //     if($ionicHistory.currentStateName !== 'tab.chat-det' && b[0].user !== $scope.user.id){
  //       showModal('Nova Mensagem','teste');
  //     }
  //   });
  // }

  // $scope.$on('$ionicView.afterEnter', function () {
  //   //alert('After Enter Cards');
  // });

  // $scope.$on('$ionicView.beforeEnter', function () {
  //   //alert('After Enter Cards');
  // });

  $scope.$on('$ionicView.enter', function(){
    //alert('Cards');

    $scope.hide.ctrl = true;
    $scope.hide.notice = true;

    database.select('user').then(function(res){
      //alert('cards db user res ' + JSON.stringify(res));

      if(res.length > 0){
        $scope.user = res.item(0);
    
        //$scope.user = UserService.getUser();
        //alert('scope user cards ' + JSON.stringify($scope.user));
        
        $scope.mymatch = {};
        $rootScope.inCards = true;
        
        if(navigator.connection.type == 'none'){
          var alertPopup = $ionicPopup.alert({
            title: 'Sem Internet',
            template: 'Habilite a conexão da internet no seu dispositivo para usar o Bailaki.'
          });
          alertPopup;
          $ionicLoading.hide();
        }else{
          $rootScope.checkPermission().then(function(){
            main_func();
          });
        
          window.plugins.OneSignal.setEmail($scope.user.email);
          // socket.connect($scope.user.email);

          // setSocket();

          cordova.plugins.diagnostic.isLocationEnabled(function(enabled){
            if(!enabled){
              cordova.plugins.locationAccuracy.canRequest(function(canRequest){
                if(canRequest){
                  cordova.plugins.locationAccuracy.request(function (success){
                    main_func();
                  }, function (error){
                    if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
                        if(window.confirm('Falha ao requerer Geolocalização automaticamente. Você gostaria de alterar manualmente?')){
                            cordova.plugins.diagnostic.switchToLocationSettings();
                        }
                    }else{
                      main_func();
                    }
                  }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
                }else{
                  $rootScope.showAlert('Erro ao obter a localização. Habilite a localização do aparelho para utilizar o Bailaki.');
                  $rootScope.checkPermission();
                }
              });
            }else{
              main_func();
            }
          }, function(error){
            $rootScope.showAlert('Erro no GPS', 'Erro ao obter a localização. Habilite a localização do aparelho para utilizar o Bailaki.');
            $rootScope.checkPermission();
          });  
        }
      }
      else{
        $state.go('tab.login');
      }
    }
    // , function(err){
    //alert(JSON.stringify(err));
    // }
    );
  });

  var opt = {timeout: 90000, maximumAge: 3600000, enableHighAccuracy: true};

  var main_func = function(){
    //alert('main_func');
    //if($scope.user.is_logged == true){
    if((typeof($rootScope.infoU) == 'undefined' || $rootScope.infoU == undefined || $rootScope.infoU == 'undefined') && $scope.cards.length == 0){
      $scope.cardsControl = {};

      $cordovaGeolocation.getCurrentPosition(opt).then(function(pos) {
        $scope.user.latitude = pos.coords.latitude;
        $scope.user.longitude = pos.coords.longitude;
        UserService.setUser($scope.user);

        database.select('buscar').then(function(res){
          //alert(JSON.stringify(res));

          if(res.length > 0){
            $scope.user.buscar = res.item(0);

            //alert(JSON.stringify(res.item(0)));

            $http.post(Url.url + '/api/saveUserPost', {user:  angular.toJson($scope.user)}).then(function(res){
              if(res.status == 200){
                var u = angular.copy($scope.user);
                u.buscar = '';
                u.fotos = u.fotos.replace(/"/g, '"');

                database.update('user', 'id', u.id, u).then(function(r){
                  distance($scope.user.buscar);
                }
                // , function(err){
                //alert(JSON.stringify(err));
                // }
                );
              }
            });
          }else{
            $state.go('tab.login');
          }

        }
        // , function(err){
        //alert(JSON.stringify(err));
        // }
        );
        // .then(function(res){
        //   //alert('segunda');

        //   $http.post(Url.url + '/api/saveUserPost', {user:  angular.toJson($scope.user)}).then(function(res){
        //     if(res.status == 200){
        //       var u = angular.copy($scope.user);
        //       u.buscar = '';
        //       u.fotos = u.fotos.replace(/'/g, ''');

        //       database.update('user', 'id', u.id, u).then(function(r){
        //         distance($scope.user.buscar);
        //       }
        //       // , function(err){
        //       //alert(JSON.stringify(err));
        //       // }
        //       );
        //     }
        //   });
        // });
      }, function(error){
        if(error.code == 2){
          $rootScope.checkPermission();
        }else{
          if(maxAge < 3620000){
            maxAge = opt.maximumAge + 10000;
            opt = {timeout: 120000, maximumAge: maxAge, enableHighAccuracy: false};
            main_func();
          }
        }
      });
    }else{
      //alert('else');
      $scope.c = $scope.cards[0];
      $scope.hide.ctrl = false;
    }
    //}else{
    //  $state.go('tab.login');
    //}
  }

  $scope.cardSwipedLeft = function(index){
    $scope.hide.ctrl = true;
    like(0);
  }

  $scope.cardSwipedRight = function(index){
    $scope.hide.ctrl = true;
    $scope.c = $scope.cards[0];
    $scope.mymatch.id = $scope.c.id;
    $scope.mymatch.nome = $scope.c.nome;
    $scope.mymatch.f = $scope.c.fotos[0];
    like(1);
  }

  function change(){
    $scope.cards.shift();
    if($scope.cards.length == 0){
      $scope.hide.ctrl = true;
      distance($scope.user.buscar);
    }else{
      $scope.c = $scope.cards[0];
      $scope.hide.ctrl = false;
    }
    $scope.$apply();
  }

  $scope.yesClick = function(e){
    e.preventDefault();
    $scope.hide.ctrl = true;
    $scope.c = $scope.cards[0];
    $scope.mymatch.id = $scope.c.id;
    $scope.mymatch.nome = $scope.c.nome;
    $scope.mymatch.f = $scope.c.fotos[0];
    like(1);
    change();
  };
  
  $scope.noClick = function(e){
    e.preventDefault();
    $scope.hide.ctrl = true;
    $scope.c = $scope.cards[0];
    like(0);
    change();
  };

  $scope.info = function(){
    //alert('INFO');
    $rootScope.infoU = $scope.c;
    $state.go('tab.info');
  }

  $scope.cardDestroyed = function(e){
    $scope.hide.ctrl = true;
    change();
  }

  $rootScope.scheduleNoti = function(text, in_time, unit){
    cordova.plugins.notification.local.schedule({
      title: text,
      trigger: { in: in_time, unit: unit},
      icon: 'https://bailaki.com/wp-content/uploads/2019/02/drawable-xxxhdpi-icon.png'
    });
  }

  var distance = function(buscar){
    $ionicLoading.show({
      template: '<ion-spinner icon="ripple" class="spinner"></ion-spinner>'
    });
    //alert('distance');
    //alert('buscar ' + JSON.stringify(buscar));

    //if(($scope.user.latitude !== undefined) && ($scope.user.longitude  !== undefined)){
    $http.post(Url.url + '/api/distance2', {user: angular.toJson($scope.user), buscar: buscar}).then(function(res){
      //alert(JSON.stringify(res));

      if(res.data.users.length == 0){
        $scope.hide.ctrl = true;
        $scope.hide.notice = false;
      }else{
        $scope.hide.notice = true;
        $scope.cards = res.data.users;
        $scope.c = res.data.users[0];
        $timeout(function(){
          $scope.hide.ctrl = false;
          $scope.$apply();
        }, 2000);
      }
      $ionicLoading.hide();
    }, function(err){
      //alert('Err distance ' + JSON.stringify(err));
      $ionicLoading.hide();
    });
    //}
  }

  function like(l){
    var info = $q.defer();

    $http.post(Url.url + '/api/like', {id: $scope.user.id, other: $scope.c.id, like: l, token: $scope.user.token}).then(function(res){
      if(res.data.match == true){
        socket.emit('match', $scope.mymatch.id, $scope.user.id, $scope.user.nome);
        $rootScope.match = $scope.mymatch;
        $state.go('tab.match');
        info.resolve(res);
      }else{
        info.resolve(res);
      }
    }, function(err){
      info.reject(err);
    });

    $rootScope.infoU = 'undefined';

    return info.promise;
  }


  // function handleErrors(err){
  //   //alert(JSON.stringify(err));
  // }


  function showModal(title, msg){
    $scope.modalMsg = $ionicModal.fromTemplate('<ion-modal-view class="modalMsg" ng-click="goChats()><ion-header-bar><img src="img/icon.png"/><h1 class="title">'+ title +'</h1></ion-header-bar></ion-modal-view>', {
      scope: $scope,
      animation: 'slide-in-up'
    });
    $scope.modalMsg.show();
    $timeout(function(){
      $scope.modalMsg.hide();
    }, 3000);
  }
  
})

.controller('MatchCtrl', function($rootScope, $scope, $state, Url, UserService){

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    //alert(JSON.stringify($scope.user));

    if(typeof($rootScope.match.f) == 'undefined'){
      $http.post(Url.url + '/api/UserPictures', {id: $scope.user.id, token: $scope.user.token, oid: $rootScope.match.id}).then(function(res){
        if(res.data.fotos.length > 0){
          $rootScope.match.f = res.data.fotos[0];
        }
      });
    }
  });

  $scope.continue = function(){
    $state.go('tab.cards');
  }

  getCid = function(){
    var cid = 0;
    if($scope.user.id < $rootScope.match.id){
      cid = '' + $scope.user.id + $rootScope.match.id;
    }else{
      cid = '' + $rootScope.match.id + $scope.user.id;
    }
    return cid;
  }

  $scope.goMsg = function(){
    var cid =getCid();
    $rootScope.chatU = {chatId: cid, id: $rootScope.match.id, nome: $rootScope.match.nome, f: $rootScope.match.f};
    $state.go('tab.chat-det');
  }

  $scope.goMap = function(){
    var cid = getCid();
    $rootScope.chatU = {chatId: cid, id: $rootScope.match.id, nome: $rootScope.match.nome, f: $rootScope.match.f};
    $state.go('tab.mapa');
  }
})

.controller('InfoCtrl',function(database, $http,$ionicHistory,$ionicPopup,$rootScope,$scope,$state,Url,UserService,$window){

  $scope.$on('$ionicView.enter', function(){
    database.select('user').then(function(res){
      $scope.user = res.item(0);
    });

    $scope.tela = {};
    $scope.tela.showUndo = false;
    $scope.data = {};
    $scope.data.fotos = [];
    $scope.u = $rootScope.infoU;

    //alert(JSON.stringify($scope.u));
    
    if($scope.u.match == true){
      $scope.tela.showUndo = true
    }
  });

  $scope.desfazer = function(){
    var confirmPopup = $ionicPopup.confirm({
      title: 'Desfazer Match',
      template: 'Tem certeza que deseja desfazer esse match? Essa ação não pode ser revertida.',
      buttons: [{
        text: 'Não',
        type: 'button-default'
      }, {
        text: 'Sim',
        type: 'button-default'
      }]
    });

    confirmPopup.then(function(res) {
      if(res) {
        $http.post(Url.url + '/api/like', {id: $scope.user.id, other: $scope.u.id, like: 0, token: $scope.user.token}).then(function(res){
          if(res.data.match == true){
            $state.go('tab.cards');
          }
        });
      }
    });
  }

  $scope.goBack = function(){
    $window.history.go(-1);
  }

  $scope.$on('$ionicView.leave', function(){
    delete $rootScope.infoU;
  });
})

.controller('ChatsCtrl', function(database, $scope, socket, $state, $http, UserService, Url, $ionicHistory, $rootScope, $ionicLoading) {
  $scope.$on('$ionicView.enter', function(){
    try{
      database.select('user').then(function(res){
        if(res.length > 0){
          $scope.user = res.item(0);
          database.select('buscar').then(function(r){
            $scope.user.buscar = r.item(0);
            UserService.setUser($scope.user);
            findMatches($scope.user);
          });
        }
      });
    }
    catch(e){
      //alert(JSON.stringify(e));
      $scope.user = UserService.getUser();
    }
    finally{
      $scope.chats = [];
      $scope.hide = {};
      $scope.hide.none = true;
      delete $rootScope.chatU;

      $scope.url = 'https://bailaki.com/imagens_bailaki/uploads/';
    }
  });
  
  $scope.goChatUnico = function(cid, i, n, f, buscar){
    $rootScope.chatU = {chatId: cid, id: i, nome: n, f: f, buscar: buscar};
    $rootScope.user_ = $scope.user;
    UserService.setUser($scope.user);
    $state.go('tab.chat-det');
  }

  $scope.goInfo = function(i, n, b, d, f){
    $rootScope.infoU = {id: i, nome: n, match: true, birthday: b, distancia: d, fotos: f};
    $state.go('tab.info');
  }

  $scope.goBack = function(){
    $ionicHistory.goBack();
  }

  $scope.goCards = function(){
    $state.go('tab.cards');
  }

  function findMatches(user){
    $ionicLoading.show();
      
    $http.post(Url.url + '/api/matches', {user: user.id, token: user.token}).then(function(res){
      $scope.chats = res.data.match;
      if(res.data.match.length == 0){
        $scope.hide.none = false;
        $rootScope.evt = false;
      }
      $ionicLoading.hide();
    }, function(error){
      $ionicLoading.hide();
    });
  }
})

.controller('ChatDetCtrl', function(database, $ionicHistory, $ionicScrollDelegate, $rootScope, $sanitize, $scope, socket, $state, UserService) {
  $scope.hide = {};
  $scope.hide.evt = true;

  function onPause() {
    socket.emit('pause');
  }

  function onResume() {
    socket.emit('resume');
  }

  $scope.$on('$ionicView.load', function(){
    $scope.user = UserService.getUser();
  })

  function insertMessage(message){
    database.insert('message', Object.keys(message).toString(), Object.values(message))
    // .then(function(r){
    //   //alert(JSON.stringify(r));
    // }, function(err){
    //   //alert(JSON.stringify(err))
    // });
  }

  $scope.$on('$ionicView.afterEnter', function(){
    setSocketListeners();
    //joinChat();
  });

  $scope.$on('$ionicView.enter', function(){
    $scope.user = $rootScope.user_;
    
    $scope.hide.evt = true;

    $scope.messages = [];
    $scope.message = '';
    $scope.url = 'https://bailaki.com/imagens_bailaki/uploads/';
    $scope.other = $rootScope.chatU;

    if($rootScope.evt){
      var evento = angular.copy($rootScope.evt);
      var est = angular.copy($rootScope.evt.estilos);
      var rits = est.map(function(i){ 
        return '<span>' + i + '</span>'; 
      });
      $scope.message = '<div class="event-speech-block" id='+ evento.id +' ng-click="showEvento()"><div class="showevento"><img src="'+ evento.fotos[0]+'"><h2>'+evento.nome +'</h2><p>'+ evento.descricao +'</p><p>'+ evento.localizacao + '</p><p>' + evento.data + ' - ' + evento.hora + '</p><p>R$ ' + evento.valor + '</p></div><div class="event-speech-tags">' + rits + '</div></div>';
      $scope.sendMessage();
      $rootScope.evt = {};
    }
  });

  $scope.sendMessage = function(){
    var msg = {text: $scope.message, user: $scope.user.id};
    //socket.emit('userJoined', $scope.other.chatId, $scope.other, $scope.user.id, $scope.user.email);
    socket.emit('message', $scope.other.chatId, $scope.other.id, encodeURI(JSON.stringify(msg)), $scope.other.buscar);
    $scope.message = '';
  }

  function addMessageToList(user_id, style_type, message){  
    //alert('addmessage ' + JSON.stringify(message));

    insertMessage(message);

    var username = $sanitize(user_id); 
    var color = style_type ? getUsernameColor(user_id) : null; 
    var f = style_type ? getUsernamePicture(user_id) : 'img/image.gif'; 

    var val = new Date(message.createdAt);

    var dd = val.getDate();
    var mm = val.getMonth()+1; //January is 0!
    var yyyy = val.getFullYear();
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    } 
    var hh = val.getHours();
    var min = val.getMinutes();
    var data = dd+'/'+mm+'/'+yyyy + ' ' + hh + ':' + min;

    $scope.messages.push({content:message.text,style:style_type,username:user_id,color:color,f:f, data: data, date: val});

    $ionicScrollDelegate.scrollBottom();
  }

  function getUsernameColor(id) {
    if(id == $scope.user.id){
      return 'right';
    }else{
      return 'left';
    }
  }

  function getUsernamePicture(username) {
    if(username == $scope.user.id){
      return $scope.user.fotos.split(',')[0];
    }else{
      return $scope.other.f;
    }
  }

  $scope.goBack = function(){
    $ionicHistory.goBack();
  }

  $scope.goCards = function(){
    $state.go('tab.cards');
  }

  $scope.showEvento = function(){
    $scope.hide.evt = false;
  }

  $scope.hideEvt = function(){
    $scope.hide.evt = true;
  }

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
    delete $rootScope.chatU;
  });

  function setSocketListeners(){
    // try{
    socket.emit('userJoined', $scope.other.chatId, $scope.other.id, $scope.user.id, $scope.user.email);

    socket.on('message', function (id, oid, data, otherBuscar) {
      for(var i=0; i < data.length; i++){
        addMessageToList(data[i].user, true, data[i]);
      }
    });

    document.addEventListener('pause', onPause, false);
    document.addEventListener('resume', onResume, false);

    socket.on('match', function(matchId, userId, userName){
      $rootScope.scheduleNoti('Novo Match', 2, 'second');
    });

      //socket.on('connect_error', err => handleErrors(err));
      //socket.on('connect_failed', err => handleErrors(err));
      //socket.on('disconnect', err => handleErrors(err));
      //socket.on('connect', err => handleErrors(err));
      //socket.on('error', err=> handleErrors(err));
    // }
    // catch(e){
    //   //alert(JSON.stringify(e))
    // }
  }
})

.controller('MapaCtrl',function($cordovaGeolocation, $http, $ionicHistory, $ionicLoading, Markers, $rootScope,$scope, $state, Url,UserService){  
  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
  });

  $scope.$on('$ionicView.beforeEnter', function(){
    $scope.loaded = false;
  });

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    delete $rootScope.evt;
    $ionicLoading.show({
      template: '<ion-spinner icon="ripple" class="spinner"></ion-spinner>'
    });

    cordova.plugins.diagnostic.isLocationEnabled(function(enabled){
      if(!enabled){
        cordova.plugins.locationAccuracy.canRequest(function(canRequest){
          if(canRequest){
            cordova.plugins.locationAccuracy.request(function (success){
              loadMap();
            }, function (error){
              if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
                if(window.confirm('Falha ao requerer Geolocalização automaticamente.')){
                  cordova.plugins.diagnostic.switchToLocationSettings();
                }
              }else{
                loadMap();
              }
            }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
          }else{
            $rootScope.showAlert('Erro','Erro ao obter a localização. Habilite a localização do aparelho para utilizar o Bailaki.');
          }
        });
      }else{
        if($scope.loaded == false){
          loadMap();
        }
      }
    }, function(error){
      $rootScope.showAlert('Erro','Erro ao obter a localização. Habilite a localização do aparelho para utilizar o Bailaki.');
    });   
  });

  var loadMap = function(){
    var opt = {timeout: 120000, enableHighAccuracy: false};
    $cordovaGeolocation.getCurrentPosition(opt).then(function(pos) {
      var myLatlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
      var mapOptions = {
          center: myLatlng,
          zoom: 11,
          minZoom: 3,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById('map'), mapOptions);
      var myLocation = new google.maps.Marker({
          position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
          map: map,
          title: 'Eu'
      });
      loadMarkers(map);
    });
    $scope.map = map;
    $scope.loaded = true;
    $ionicLoading.hide();
  }

  function loadMarkers(map){
    if($rootScope.filtro){
      var ritmos = $rootScope.filtro.ritmos;
    }else{
      var ritmos = [];
    }

    Markers.getMarkers(ritmos).then(function(markers){
      for (var i = 0; i < markers.data.eventos.length; i++) {
        var rec = markers.data.eventos[i];
        var marker = new google.maps.Marker({
          map: map,
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(rec.lat, rec.lng),
          icon: 'img/map-marker.png'
        });
        verEvento(marker, rec);
      }
    });
  }
 
  function verEvento(marker, rec) {
    google.maps.event.addListener(marker, 'click', function () {
      $rootScope.evt = rec;
      $state.go('tab.evento');
    });
  }
})

.controller('FiltroEvtCtrl',function($state, $scope, $http, $rootScope, Ritmos, UserService, Url, $ionicHistory){

  $scope.ritmos = Ritmos;

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    $scope.ritmos = $scope.ritmos.map()

    if(typeof($rootScope.filtro.ritmos) != 'undefined'){
      angular.forEach($scope.ritmos, function(value, key) {
        var index = $rootScope.filtro.ritmos.indexOf(value.nome);
        if (index > -1) {
          value.checked = true;
        }else{
          value.checked = false;
        }
      });
    }
  });
  
  $scope.selectRit = function(r){
    if($rootScope.filtro == undefined){
      $rootScope.filtro = {};
      $rootScope.filtro.ritmos = [];
    }
    if($rootScope.filtro.ritmos.indexOf(r.nome) > -1){
      $rootScope.filtro.ritmos.splice(idx, 1);
    }else{
      $rootScope.filtro.ritmos.push(r.nome);
    }
  }
  
  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
  });
})

.controller('EventoCtrl', function($http, $rootScope, $state, $scope, Url, UserService, $ionicHistory){
  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
  });

  $scope.inviteMatch = function(){
    if($rootScope.chatU){
      $state.go('tab.chat-det');
    }else{
      $state.go('tab.chats');
    }
  }

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
  });
})

.controller('AddEventCtrl',function($cordovaCamera, $cordovaDevice, $cordovaFile, $cordovaFileTransfer, $http, $ionicActionSheet, $ionicHistory, $ionicLoading, $ionicPopup, $ionicScrollDelegate, $q, $rootScope, $scope, $state, $timeout, Url, UserService){

  $scope.$on('$ionicView.enter', function(){
    $ionicScrollDelegate.scrollTop();
    $scope.user = UserService.getUser();
    $scope.hide = {};
    $scope.ok = {};
    $scope.ok.disabled = true;

    if($rootScope.evento){
      $scope.evento = $rootScope.evento;
      $scope.checkFields();
    }else{
      $scope.evento = {};
      $scope.evento.data = '';
      $scope.evento.hora = '';
      $scope.evento.nome = '';
      $scope.evento.descricao = '';
      $scope.evento.valor = '';
      $scope.evento.localizacao = '';
      $scope.evento.ritmos = [];
      $scope.evento.images = [];
      $scope.evento.convidados = '';
      $scope.evento.preco = '';
      $rootScope.selRit = false;
      $rootScope.evento = $scope.evento
    }
  });

  $scope.goRitmosEvt = function(){
    UserService.setUser($scope.user);
    $rootScope.evento = $scope.evento;
    $state.go('tab.ritmos-evt');
  }

  $scope.$on('$ionicView.leave', function(){
    $rootScope.evento = $scope.evento;
    //$scope.evento = {};
    UserService.setUser($scope.user);
  });

  $scope.checkFields = function(){
    if($scope.evento.nome !== '' && $scope.evento.descricao !== '' && $scope.evento.data !== '' && $scope.evento.hora !== '' && $scope.evento.valor !== '' && $scope.evento.localizacao !== '' && $scope.evento.convidados !== '' && $scope.evento.preco !== ''){
      $scope.ok.disabled = false;
    }else{
      $scope.ok.disabled = true;
    }
  }

  $scope.Remover = function(idx){
    $scope.evento.images.splice(idx, 1);
  }

  function moveFile(fileUri) {
    window.resolveLocalFileSystemURL(
      fileUri,
      function(fileEntry){
        window.resolveLocalFileSystemURL(
          cordova.file.dataDirectory,
          function(dirEntry) {
            var newFilename = 'b-' + Date.parse(new Date()).toString() + '.jpg';
            fileEntry.moveTo(dirEntry, newFilename, function(res){
              onSuccess(res);
            });
          });
      });
  }

  function onSuccess(res){
    var img = res.toURL();
    var pic = {img: img, num: $scope.evento.images.length};
    $scope.evento.images.push(pic);
    $scope.$apply();
  }

  $scope.getFoto = function(){
    if($scope.evento.images.length < 3){
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 100,
        targetHeight: 100,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function(imagePath){
        moveFile(imagePath);
      });
    }else{
      $rootScope.showAlert('Erro','Apenas 3 fotos podem ser adicionadas.');
    }
  }

  $scope.goAddCreditos = function(){
    $rootScope.evento = $scope.evento;
    $state.go('tab.add-creditos');
  }

  $scope.calculatePrice = function(){
    if($scope.evento.convidados > 0){
      calcularPreco().then(function(res){
        $scope.checkFields();
      });
    }else{
      $scope.checkFields();
    }
  }

  var calcularPreco = function(){
    var deferred = $q.defer();
    $ionicLoading.show();
    
    $http.post(Url.url + '/api/postProductByGuestsNumber', {number: $scope.evento.convidados}).then(function(res){
      if(res.data.produto.length > 0){
        $scope.evento.preco = res.data['produto'][0][2];
        deferred.resolve(res.data);
      }else{
        deferred.reject(err);
      }
      $ionicLoading.hide();
    }, function(err){
      $ionicLoading.hide();
      deferred.reject(err);
    });
    return deferred.promise;
  }
})

.controller('RitmosEvtCtrl',function($state, $scope, $http, UserService, Ritmos, $rootScope){
  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    $scope.ritmos = Ritmos;
    
    if(typeof($rootScope.evento.ritmos) == 'undefined' || $rootScope.evento.ritmos == null || JSON.stringify($rootScope.evento) == '{}'){
      angular.forEach($scope.ritmos, function(value, key) {
        value.checked = false;
      });
      $rootScope.evento.ritmos = [];
      $scope.$apply();
    }
  });

  $scope.selectRit = function(r){
    var sel = false;
    
    angular.forEach($scope.ritmos, function(value, key) {
      if(r.id == value.id){
        value.checked = !value.checked;
        if(value.checked == true){
          r.checked = true;
          $rootScope.evento.ritmos.push(value.nome);
          sel = true;
        }else{
          var index = $rootScope.evento.ritmos.indexOf(value.nome);
          if (index > -1) {
            $rootScope.evento.ritmos.splice(index, 1);
          }
        }
      }
    });

    if(sel == false){
      $scope.ok.disabled = true;
    }
  }

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
  });
})

.controller('AddCreditosCtrl',function($http, $ionicLoading, $ionicScrollDelegate, $rootScope, $scope, $state, UserService){
  var hash = '';

  $scope.bandeiras = ['visa', 'mastercard','amex','diners','hipercard', 'elo'];
  $scope.ok = {};
  $scope.erro = {};

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    //alert(JSON.stringify($scope.user));
    $ionicScrollDelegate.scrollTop();

    $scope.evento = $rootScope.evento;

    if($rootScope.usuario){
      $scope.usuario = $rootScope.usuario;
    }else{
      $scope.usuario = {
        cartao: '',
        cvv: '',
        validade: '',
        bandeira: ''
      };
    }

    $scope.ok.disabled = true;
    $scope.ok.cartao = false;
    $scope.checkFields();

    $http.post('https://bailaki.com/public/api/token_pagseguro').then(function(res){
      PagSeguroDirectPayment.setSessionId(res.data.xml.id);
    });
  });

  PagSeguroDirectPayment.onSenderHashReady(function(response){
    if(response.status == 'error') {
      return false;
    }
    hash = response.senderHash;
  });

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
  });

  $scope.checkFields = function(){
    if($scope.usuario.cartao !== '' && $scope.usuario.cvv !== '' && $scope.usuario.validade !== '' && $scope.usuario.bandeira !== ''){
      $scope.ok.disabled = false;
    }else{
      $scope.ok.disabled = true;
    }
  }

  $scope.getBandeira = function(){
    PagSeguroDirectPayment.getBrand({
      cardBin: $scope.usuario.cartao.substr(0,6),
      success: function(response) {
        $scope.usuario.bandeira = response.brand.name;
        $scope.ok.cartao = true;
        $scope.erro.msg = '';
      },
      error: function(err) {
        $scope.erro.msg = 'Cartão Inválido';
        $scope.ok.cartao = false;
        $scope.usuario.bandeira = '';
      }
    });
  }

  $scope.validarCartao = function(){
    if($rootScope.evento.preco != 0.00){
      PagSeguroDirectPayment.createCardToken({
        cardNumber: $scope.usuario.cartao, // Número do cartão de crédito
        brand: $scope.usuario.bandeira, // Bandeira do cartão
        cvv: $scope.usuario.cvv, // CVV do cartão
        expirationMonth: $scope.usuario.validade.substr(0,2), // Mês da expiração do cartão
        expirationYear: $scope.usuario.validade.substr(3,7), // Ano da expiração do cartão, é necessário os 4 dígitos.
        success: function(response) {
          if(response.error === true){
            $rootScope.showAlert('Cartão Inválido','Dados inválidos');
          }else{
            if(response.card.token){
              $rootScope.usuario = $scope.usuario;
              $rootScope.usuario.hash = hash;
              $rootScope.usuario.cardToken = response.card.token;
              $state.go('tab.dados-dono-cartao');
            }
          }
        }
      });
    }
  }
})

.controller('DadosDonoCartaoCtrl', function($cordovaFileTransfer, $http, $ionicLoading, $ionicPopup, $q, $rootScope, $scope, $state, $timeout, UserService, Url){
  $scope.estados = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

  $scope.$on('$ionicView.enter', function(){
    $scope.user = UserService.getUser();
    //alert(JSON.stringify($scope.user));
    $scope.usuario = $rootScope.usuario;
  });

  $scope.$on('$ionicView.leave', function(){
    UserService.setUser($scope.user);
  });

  $scope.comprar = function(){
    var user = angular.copy($scope.user);
    user.fotos = null;
    user.ritmos = null;
    user.buscar = null;

    $http.post(Url.url + '/api/comprarEvt',{user: angular.toJson(user), usuario: angular.toJson($scope.usuario), evento: angular.toJson($rootScope.evento)}).then(function(res){
      if(res.data.res == 1){
        uploadImage(user, res.data.id, $rootScope.evento.images);
        $rootScope.showAlert('Parabéns', 'Evento adicionado. Aguarde a moderação.');
        delete $rootScope.evento;
        $state.go('tab.mapa');
      }
    }, function(err){
      $rootScope.showAlert('Erro','Erro ao processar pagamento.');
    });
  }

  function setParametros(user, image, id){
    var targetPath = image.img;
    var filename = targetPath.split('/').pop();
    const dados = {
      evento_id: id,
      user: user.id,
      token: user.token
    }
    var options = {
      fileKey: 'file',
      fileName: filename,
      chunkedMode: false,
      mimeType: 'image/jpg',
      httpMethod: 'POST',
      params: {data: dados}
    };

    return [targetPath, options];
  }

  function uploadImage(user, evento_id, images){
    var url = Url.url + '/api/fileUpload';

    if(images.length > 0){

      const dados = {
        evento_id: evento_id,
        user: user.id,
        token: user.token
      }

      $q.all(images.map(function(image) {
        var targetPath = image.img;
        var filename = targetPath.split('/').pop();
        var options = {
          fileKey: 'file',
          fileName: filename,
          chunkedMode: false,
          mimeType: 'image/jpg',
          httpMethod: 'POST',
          params: {data: dados}
        };

        //alert(options.filename);

        return $cordovaFileTransfer.upload(url, targetPath, options).then(function(res){
          return res.data;
        });
      })).then(function(data) {
        return data;
      });
    }
  };

  $scope.buscarCep = function(){
    $http.get('https://viacep.com.br/ws/' + $scope.usuario.cep + '/json/').then(function(res){
      $scope.usuario.endereco = res.data.logradouro;
      $scope.usuario.bairro = res.data.bairro;
      $scope.usuario.cidade = res.data.localidade;
      $scope.usuario.estado = res.data.uf;
    });
  }
});