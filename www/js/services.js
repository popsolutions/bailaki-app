angular.module('starter.services', [])

.service('database', function($cordovaSQLite, $ionicPlatform, $q){
  const getDB = function(){
    return $cordovaSQLite.openDB({name: "bailaki.db", location: 'default'});
  }

  const createTables = function() {
    $ionicPlatform.ready(function(){
      const db = getDB();

      var query = 'CREATE TABLE IF NOT EXISTS user (';
      query += 'id INTEGER PRIMARY KEY,';
      query += 'email TEXT,';
      query += 'social_id TEXT,';
      query += 'social_network TEXT,';
      query += 'nome TEXT,';
      query += 'accessToken TEXT,';
      query += 'is_logged TEXT,';
      query += 'fotos TEXT,';
      query += 'about TEXT,';
      query += 'ritmos TEXT,';
      query += 'sexo TEXT,';
      query += 'created_at TEXT,';
      query += 'updated_at TEXT,';
      query += 'deleted_at TEXT,';
      query += 'telefone TEXT,';
      query += 'latitude TEXT,';
      query += 'longitude TEXT,';
      query += 'birthday TEXT,';
      query += 'work TEXT,';
      query += 'escolaridade TEXT,';
      query += 'nivel TEXT,';
      query += 'creditos TEXT,';
      query += 'token TEXT,';
      query += 'buscar TEXT,';
      query += 'raio TEXT);';

      $cordovaSQLite.execute(db,query);
      // .then(function(res) {
      //   alert(JSON.stringify(res));
      // }, function (err) {
      //   alert(JSON.stringify(err));
      // });

      $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS buscar (user_id INTEGER, age_min INTEGER, age_max INTEGER, sexo_m BOOLEAN, sexo_f BOOLEAN, sexo TEXT, config TEXT, noti_msg BOOLEAN, noti_match BOOLEAN, niveis TEXT, ritmos TEXT);');
      
      // .then(function(res) {
      //   alert(JSON.stringify(res));
      // }, function (err) {
      //   alert(JSON.stringify(err));
      // });
      // $cordovaSQLite.execute(db,'CREATE TABLE IF NOT EXISTS matches (id integer primary key NOT NULL, user1 INTEGER, user2 INTEGER)');

      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS message (_id TEXT UNIQUE PRIMARY KEY NOT NULL, text TEXT, user INTEGER, createdAt TEXT, chatId TEXT)");
    });
  } 

  const insert = function (table, cols, values) {
    var d = $q.defer();

    const placeholders = values.map(function(i){
      return '?';
    });

    var query = "INSERT INTO " + table + " (" + cols + ") VALUES ("+ placeholders.join(',') +")";

    // alert('values ' + JSON.stringify(values));
    // alert('colr ' + cols);

    const db = getDB();

    $ionicPlatform.ready(function(){
      $cordovaSQLite.execute(db, query, values)
        .then(function(res) {
          d.resolve(res.insertId);
        }, function (err) {
          d.reject(err);
        });
    });

    return d.promise;
  }

  const select = function(table){
    var d = $q.defer();

    // alert('select ' + table);

    const query = "SELECT * FROM " + table;
    const db = getDB();

    $ionicPlatform.ready(function(){
      $cordovaSQLite.execute(db, query)
        .then(function(res) {
          d.resolve(res.rows);
        }, function (err) {
          d.reject(err);
        });
    });

    return d.promise;
  }

  const update = function(table, prop, value, object){
    var d = $q.defer();

    const placeholders = Object.keys(object).map(function(i){
      if (typeof(object[i]) == 'integer'){
        return i + '=' + object[i] + ' ';
      }else{
        return i + '="' + object[i] + '"';
      }
    });

    var query = "UPDATE " + table + " SET " + placeholders.join(',') + " WHERE " + prop + "=" + value;

    //alert('query ' + query);
    
    const db = getDB();

    $cordovaSQLite.execute(db, query)
      .then(function(res) {
        d.resolve(res);
      }, function (err) {
        d.reject(err);
      });

    return d.promise;
  }

  const deleteDB = function(){
    var d = $q.defer();

    $ionicPlatform.ready(function(){
      //alert('deleteDB');

      $cordovaSQLite.deleteDatabase({name: "bailaki.db", location: 'default'}).then(function(res){
        //alert(JSON.stringify(res));
        d.resolve(res);
      }, function(err){
        alert(JSON.stringify(err));
        d.reject(err);
      });
    });

    return d.promise;
  }

  const dropTable = function(table){
    var d = $q.defer();

    const db = getDB();

    var query = "DROP TABLE IF EXISTS " + table;
 
    //alert(query);

    $cordovaSQLite.execute(db, query)
      .then(function(res) {
        //alert('clearTable ' + JSON.stringify(res));
        d.resolve(res);
      }, function (err) {
        d.reject(err);
      });

    return d.promise;
  }

  return {
    createTables: createTables,
    getDB: getDB,
    insert: insert,
    select: select,
    update: update,
    deleteDB: deleteDB,
    dropTable: dropTable
  };
})


.service('UserService', function(){
  var setUser = function(user_data) {
    window.localStorage.user_bailaki = JSON.stringify(user_data);
  };

  var getUser = function(){
    return JSON.parse(window.localStorage.user_bailaki || '{}');
  };

  var empty = function(){
    window.localStorage.user_bailaki = '{}';
    window.localStorage.clear();
    window.localStorage.removeItem('user_bailaki');
    localStorage.clear();
  }

  return {
    getUser: getUser,
    setUser: setUser,
    empty: empty
  };
})

.service('Url', function(){
  return {
    url: 'https://bailaki.com/public', 
    storage: 'https://bailaki.com/storage', 
    uploads: 'https://bailaki.com/imagens_bailaki/uploads',
    chat: 'http://chat.bailaki.com.br'
  };
})

.service('Ritmos', function(){

  var ritmos = [
    {
      id: 1,
      nome: "Axé",
      checked: false
    }, 
    {
      id: 2,
      nome: "Bachata",
      checked: false
    }, 
    {
      id: 3,
      nome: "Bolero",
      checked: false
    }, 
    {
      id: 4,
      nome: "Country & Sertanejo",
      checked: false
    }, 
    {
      id: 5,
      nome: "Dança de Rua",
      checked: false
    },
    {
      id: 6,
      nome: "Dança de Salão",
      checked: false
    },
    {
      id: 7,
      nome: "Eletrônico",
      checked: false
    }, 
    {
      id: 19,
      nome: "Flashback",
      checked: false
    }, 
    {
      id: 8,
      nome: "Forró",
      checked: false
    }, 
    {
      id: 9,
      nome: "Funk",
      checked: false
    }, 
    {
      id: 10,
      nome: "Gafieira",
      checked: false
    }, 
    {
      id: 11,
      nome: "Hip Hop",
      checked: false
    },
    {
      id: 12,
      nome: "Reggaeton",
      checked: false
    },
    {
      id: 18,
      nome: "Rock",
      checked: false
    }, 
    {
      id: 13,
      nome: "Salsa & Merengue",
      checked: false
    }, 
    {
      id: 14,
      nome: "Samba & Pagode",
      checked: false
    }, 
    {
      id: 15,
      nome: "Tango",
      checked: false
    }, 
    {
      id: 16,
      nome: "Vanerão",
      checked: false
    }, 
    {
      id: 17,
      nome: "Zouk",
      checked: false
    }
  ];

  return ritmos;
})

.factory('Markers', function($http, Url) {
  var markers = [];
 
  return {
    getMarkers: function(ritmos){
      return $http.post(Url.url + '/api/Eventos', {ritmos: JSON.stringify(ritmos)}).then(function(response){
        markers = response;
        return markers;
      });
    }
  }
})


.factory('sockett',function(socketFactory, database, $rootScope, Url){

  let user = {};

  async function getU(email){
    let u = {};
    try{
      let b = await database.select('buscar');
      if(b.length > 0){
        let buscar = b.item(0);
        u = {
          id: buscar.user_id, 
          email: email, 
          buscar: {
            noti_msg: buscar.noti_msg, 
            noti_match: buscar.noti_match
          }
        };
      }
    }
    // catch(e){
    //   alert(JSON.stringify(e));
    // }
    finally{
      //alert('uuu ' + JSON.stringify(u));
      return u;
    }
  }

  async function setSocket(email){
    try{
      if(!user.hasOwnProperty(email) || (user.email !== email)){
        user = await getU(email);
      }
    }
    catch(e){
      alert(JSON.stringify(e))
    }
    finally{
      return myIoSocket = io.connect(Url.chat, {query: "user=" + angular.toJson(user) });
    }
  }

  return {
    setSocket: setSocket,
    socket: socketFactory({
      ioSocket: setSocket
    })
  };
})


.factory('socket',function(socketFactory, database, Url, UserService){

  let user = UserService.getUser();

  var _user = {
    id: user.id, 
    email: user.email, 
    buscar: {
      noti_msg: user.buscar.noti_msg, 
      noti_match: user.buscar.noti_match
    }
  };

  // var _user = {
  //   id: 1250, 
  //   email: 'jaquechocolate@hotmail.com', 
  //   buscar: {
  //     noti_msg: true, 
  //     noti_match: true
  //   }
  // };

  return socketFactory({
      ioSocket: io.connect(Url.chat, {query: "user=" + angular.toJson(_user) })
    });
})