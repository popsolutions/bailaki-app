var db = null;

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ionic-datepicker','ionic-timepicker', 'ionic.contrib.ui.tinderCards', 'btford.socket-io', 'ngSanitize','ionic-modal-select', 'ui.utils.masks', 'rzSlider'])

.run(function($ionicHistory, $cordovaSQLite, $ionicPlatform, $state, $rootScope, database) {
  $rootScope.inCards = false;
  
  $ionicPlatform.ready(function() {
    database.createTables();

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    var notificationOpenedCallback = function(jsonData) {
      $state.go('tab.chats');
    };

    window.plugins.OneSignal
      .startInit('ca132273-4fb5-4c2c-9d08-2ecbe0bcb216')
      .handleNotificationOpened(notificationOpenedCallback)
      .endInit();
  });

  //To Conditionally Disable Back
  $ionicPlatform.registerBackButtonAction(function(e){
    if($state.current.name === 'tab.login'){
      e.preventDefault();
      if($rootScope.leaveapp){
        navigator.app.exitApp();
      }else{
        $rootScope.leaveapp = true;
      }
    }
    else if($state.current.name === "tab.autorizar"){
      e.preventDefault();
    }
    else if($state.current.name === "tab.editar"){
      $state.go('tab.perfil');
    }
    else if($state.current.name === "tab.chat-det"){
      $state.go('tab.chats');
    }
    else if($state.current.name === "tab.sel-sexo" || $state.current.name === "tab.sel-data"|| $state.current.name === "tab.sel-estilos" || $state.current.name === "tab.sel-nivel" || $state.current.name === "tab.sel-escolaridade" || $state.current.name === "tab.config" || $state.current.name === "tab.add-event" || $state.current.name === "tab.ritmos" || $state.current.name === "tab.ritmos-evt" || $state.current.name === "tab.add-creditos" || $state.current.name === "tab.info" || $state.current.name === "tab.filtro-evt" || $state.current.name == "tab.evento" || $state.current.name == "tab.albums" || $state.current.name == "tab.fotos" || $state.current.name === "tab.ritmos-s" || $state.current.name === "tab.match" || $state.current.name == "tab.dados-dono-cartao"){
      $ionicHistory.goBack();
    }
    else{
      navigator.app.exitApp();
    }
  }, 100);

})

.config(function($stateProvider, $urlRouterProvider, $provide) {

  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('tab.cards', {
    url: '/cards',
    views: {
      'tab-cards': {
        templateUrl: 'templates/tab-cards.html',
        controller: 'CardsCtrl'
      }
    }
  })

  .state('tab.match', {
    url: '/match',
    views: {
      'tab-match': {
        templateUrl: 'templates/tab-match.html',
        controller: 'MatchCtrl'
      }
    }
  })

  .state('tab.sel-sexo', {
    url: '/sel-sexo',
    views: {
      'tab-sel-sexo': {
        templateUrl: 'templates/tab-sel-sexo.html',
        controller: 'SelSexoCtrl'
      }
    }
  })

  .state('tab.sel-escolaridade', {
    url: '/sel-escolaridade',
    views: {
      'tab-sel-escolaridade': {
        templateUrl: 'templates/tab-sel-escolaridade.html',
        controller: 'SelEscolaridadeCtrl'
      }
    }
  })

  .state('tab.sel-data', {
    url: '/sel-data',
    views: {
      'tab-sel-data': {
        templateUrl: 'templates/tab-sel-data.html',
        controller: 'SelDataCtrl'
      }
    }
  })

  .state('tab.sel-estilos', {
    url: '/sel-estilos',
    views: {
      'tab-sel-estilos': {
        templateUrl: 'templates/tab-sel-estilos.html',
        controller: 'SelEstilosCtrl'
      }
    }
  })

  .state('tab.sel-nivel', {
    url: '/sel-nivel',
    views: {
      'tab-sel-nivel': {
        templateUrl: 'templates/tab-sel-nivel.html',
        controller: 'SelNivelCtrl'
      }
    }
  })

  .state('tab.chats', {
    url: '/chats',
    views: {
      'tab-chats': {
        templateUrl: 'templates/tab-chats.html',
        controller: 'ChatsCtrl'
      }
    }
  })

  .state('tab.chat-det', {
    url: '/chat-det',
    views: {
      'tab-chat-det': {
        templateUrl: 'templates/tab-chat-det.html',
        controller: 'ChatDetCtrl'
      }
    }
  })

  .state('tab.perfil', {
    url: '/perfil',
    views: {
      'tab-perfil': {
        templateUrl: 'templates/tab-perfil.html',
        controller: 'PerfilCtrl'
      }
    }
  })

  .state('tab.mapa', {
    url: '/mapa',
    views: {
      'tab-mapa': {
        templateUrl: 'templates/tab-mapa.html',
        controller: 'MapaCtrl'
      }
    }
  })

  .state('tab.evento', {
    url: '/evento',
    views: {
      'tab-evento': {
        templateUrl: 'templates/tab-evento.html',
        controller: 'EventoCtrl'
      }
    }
  })

  .state('tab.config', {
    url: '/config',
    views: {
      'tab-config': {
        templateUrl: 'templates/tab-config.html',
        controller: 'ConfigCtrl'
      }
    }
  })

  .state('tab.ritmos', {
    url: '/ritmos',
    views: {
      'tab-ritmos': {
        templateUrl: 'templates/tab-ritmos.html',
        controller: 'RitmosCtrl'
      }
    }
  })

  .state('tab.editar', {
    url: '/editar',
    views: {
      'tab-editar': {
        templateUrl: 'templates/tab-editar.html',
        controller: 'EditarCtrl'
      }
    }
  })

  .state('tab.albums', {
    url: '/albums',
    views: {
      'tab-albums': {
        templateUrl: 'templates/tab-albums.html',
        controller: 'AlbumsCtrl'
      }
    }
  })

  .state('tab.fotos', {
    url: '/fotos',
    views: {
      'tab-fotos': {
        templateUrl: 'templates/tab-fotos.html',
        controller: 'FotosCtrl'
      }
    }
  })

  .state('tab.add-creditos', {
    url: '/add-creditos',
    views: {
      'tab-add-creditos': {
        templateUrl: 'templates/tab-add-creditos.html',
        controller: 'AddCreditosCtrl'
      }
    }
  })

  .state('tab.dados-dono-cartao',{
    url: '/dados-dono-cartao',
    views: {
      'tab-dados-dono-cartao': {
        templateUrl: 'templates/tab-dados-dono-cartao.html',
        controller: 'DadosDonoCartaoCtrl'
      }
    }
  })

  .state('tab.add-event', {
    url: '/add-event',
    views: {
      'tab-add-event': {
        templateUrl: 'templates/tab-add-event.html',
        controller: 'AddEventCtrl'
      }
    }
  })

  .state('tab.info', {
    url: '/info',
    views: {
      'tab-info': {
        templateUrl: 'templates/tab-info.html',
        controller: 'InfoCtrl'
      }
    }
  })

  .state('tab.ritmos-evt', {
    url: '/ritmos-evt',
    views: {
      'tab-ritmos-evt': {
        templateUrl: 'templates/tab-ritmos-evt.html',
        controller: 'RitmosEvtCtrl'
      }
    }
  })  

  .state('tab.ritmos-s', {
    url: '/ritmos-s',
    views: {
      'tab-ritmos-s': {
        templateUrl: 'templates/tab-ritmos-s.html',
        controller: 'RitmosSCtrl'
      }
    }
  })

  .state('tab.filtro-evt', {
    url: '/filtro-evt',
    views: {
      'tab-filtro-evt': {
        templateUrl: 'templates/tab-filtro-evt.html',
        controller: 'FiltroEvtCtrl'
      }
    }
  })

  .state('tab.login', {
    url: '/login',
    views: {
      'tab-login': {
        templateUrl: 'templates/tab-login.html',
        controller: 'LoginCtrl'
      }
    }
  })

  .state('tab.autorizar', {
    url: '/autorizar',
    views: {
      'tab-autorizar': {
        templateUrl: 'templates/tab-autorizar.html',
        controller: 'AutorizarCtrl'
      }
    }
  })

  
  .state('tab.cadastrar', {
    url: '/cadastrar',
    views: {
      'tab-cadastrar':{
        templateUrl: 'templates/tab-cadastrar.html',
        controller: 'CadastrarCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/login');

  $provide.decorator('$locale', ['$delegate', function ($delegate) {
      $delegate.NUMBER_FORMATS.DECIMAL_SEP = ',';
      return $delegate;
  }]);

})

// .constant('$ionicLoadingConfig', {
//   noBackdrop: true,
//   template: '<ion-spinner icon="bubbles" class="spinner"></ion-spinner>'
// })

.directive('noScroll', function() {
  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {
        $element.on('touchmove', function(e) {
            e.preventDefault();
        });
    }
  }
})

.directive('uiMultiRange', function($compile) {
    var directive = {
        restrict: 'E',
        scope: {
            ngModelMin: '=',
            ngModelMax: '=',
            ngMin: '=',
            ngMax: '=',
            ngStep: '=',
            ngChangeMin: '&',
            ngChangeMax: '&'
        },
        link: link
    };

    return directive;

    ////////////////////

    function link ($scope, $element, $attrs) {
        var min, max, step, $inputMin = angular.element('<input type="range">'), $inputMax;
        $scope.ngChangeMin = $scope.ngChangeMin || angular.noop;
        $scope.ngChangeMax = $scope.ngChangeMax || angular.noop;

        if (typeof $scope.ngMin == 'undefined') {
            min = 0;
        } else {
            min = $scope.ngMin;
            $inputMin.attr('min', min);
        }
        if (typeof $scope.ngMax == 'undefined') {
            max = 0;
        } else {
            max = $scope.ngMax;
            $inputMin.attr('max', max);
        }
        if (typeof $scope.ngStep == 'undefined') {
            step = 0;
        } else {
            step = $scope.ngStep;
            $inputMin.attr('step', step);
        }
        $inputMax = $inputMin.clone();
        $inputMin.attr('ng-model', 'ngModelMin');
        $inputMax.attr('ng-model', 'ngModelMax');
        $compile($inputMin)($scope);
        $compile($inputMax)($scope);
        $element.append($inputMin).append($inputMax);
        $scope.ngModelMin = $scope.ngModelMin || min;
        $scope.ngModelMax = $scope.ngModelMax || max;

        $scope.$watch('ngModelMin', function (newVal, oldVal) {
            if (newVal > $scope.ngModelMax - 2) {
                $scope.ngModelMin = oldVal;
            } else {
                $scope.ngChangeMin();
            }
        });

        $scope.$watch('ngModelMax', function (newVal, oldVal) {
            if (newVal < $scope.ngModelMin + 3) {
                $scope.ngModelMax = oldVal;
            } else {
                $scope.ngChangeMax();
            }
        });
    }
})

.directive('ionMultipleSelect', ['$ionicModal', '$ionicGesture', function ($ionicModal, $ionicGesture) {
  return {
    restrict: 'E',
    scope: {
      options : "="
    },
    controller: function ($scope, $element, $attrs) {
      $scope.multipleSelect = {
        title:            $attrs.title || "Selecione",
        tempOptions:      [],
        keyProperty:      $attrs.keyProperty || "id",
        valueProperty:    $attrs.valueProperty || "value",
        selectedProperty: $attrs.selectedProperty || "selected",
        templateUrl:      $attrs.templateUrl || 'templates/multipleSelect.html',
        renderCheckbox:   $attrs.renderCheckbox ? $attrs.renderCheckbox == "true" : true,
        animation:        $attrs.animation || 'slide-in-up'
      };

      $scope.OpenModalFromTemplate = function (templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
          scope: $scope,
          animation: $scope.multipleSelect.animation
        }).then(function (modal) {
          $scope.modal = modal;
          $scope.modal.show();
        });
      };
      
      $ionicGesture.on('tap', function (e) {
       $scope.multipleSelect.tempOptions = $scope.options.map(function(option){
         var tempOption = { };
         tempOption[$scope.multipleSelect.keyProperty] = option[$scope.multipleSelect.keyProperty];
         tempOption[$scope.multipleSelect.valueProperty] = option[$scope.multipleSelect.valueProperty];
         tempOption[$scope.multipleSelect.selectedProperty] = option[$scope.multipleSelect.selectedProperty];
         
         return tempOption;
       });
        $scope.OpenModalFromTemplate($scope.multipleSelect.templateUrl);
      }, $element);
      
      $scope.saveOptions = function(){
        for(var i = 0; i < $scope.multipleSelect.tempOptions.length; i++){
          var tempOption = $scope.multipleSelect.tempOptions[i];
          for(var j = 0; j < $scope.options.length; j++){
            var option = $scope.options[j];
            if(tempOption[$scope.multipleSelect.keyProperty] == option[$scope.multipleSelect.keyProperty]){
              option[$scope.multipleSelect.selectedProperty] = tempOption[$scope.multipleSelect.selectedProperty];
              break;
            }
          }
        }
        $scope.closeModal();
      };
      
      $scope.closeModal = function () {
        $scope.modal.remove();
      };

      $scope.$on('$destroy', function () {
          if ($scope.modal){
              $scope.modal.remove();
          }
      });
    }
  };
}])

.directive('format', function ($filter) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            if (!ctrl) return;

            element.bind("change", function () {
                var result = $filter('currency')(ctrl.$viewValue, {
                                                                prefix: 'R$ ',
                                                                centsSeparator: ',',
                                                                thousandsSeparator: '.'
                                                            });
                ctrl.$setViewValue(result);
                ctrl.$render();
            })

        }
    };
})

.filter('ageFilter', function() {
  function calculateAge(birthday){
    var d = new Date(birthday);
    var ageDifMs = Date.now() - d.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  return function(birthdate) {
    return calculateAge(birthdate);
  };
});
